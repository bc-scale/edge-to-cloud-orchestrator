//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 04 Aug 2021
//
// @author: ATOS
//
package cfg

import (
	"strings"
	
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
)

// Paths

/*
PathKubernetesDeployment ...
*/
const PathKubernetesDeployment = "/apis/apps/v1/namespaces/default/deployments"

/*
PathKubernetesService ...
*/
const PathKubernetesService = "/api/v1/namespaces/default/services"

/*
PathKubernetesPodsApp ...
*/
const PathKubernetesPodsApp = "/api/v1/namespaces/default/pods?labelSelector=app="

/*
PathKubernetesPod ...
*/
const PathKubernetesPod = "/api/v1/namespaces/default/pods"

/*
PathOpenshiftRoute ...
*/
const PathOpenshiftRoute = "/apis/route.openshift.io/v1/namespaces/default/routes/route-"

/*
PathOpenshiftRoutes "/apis/route.openshift.io/v1/namespaces/"+namespace+"/routes"
*/
const PathOpenshiftRoutes = "/apis/route.openshift.io/v1/namespaces/default/routes"

// URLs
var defaultRESTAPIEndPoint string

/*
Initialize ...
*/
func InitializeURLs() {
	log.Info(pathLOG + "URLs [InitializeURLs] Setting default routes ...")
	defaultRESTAPIEndPoint = Config.DefaultRESTAPIEndPoint
}

///////////////////////////////////////////////////////////////////////////////
// HOSTIP

/*
GetHostIP Return the Host IP or the value of  cfg.Config.Orchestrators[0].IP if orchestrator is nil
*/
func GetHostIP(orchestrator structs.E2cOrchestrator) string {
	ip := ""
	if orchestrator == (structs.E2cOrchestrator{}) {
		ip = Config.Orchestrators[0].E2cOrchestrator.IP
		log.Error(pathLOG + "URLs > [GetHostIP] ERROR orchestrator is nil. Returning default orchestrator Host IP [" + ip + "] ...")
	} else {
		ip = orchestrator.IP
		log.Info(pathLOG + "URLs > [GetHostIP] Host IP: " + ip)
	}
	return ip
}

// KUBERNETES

// Deployments

/*
GetPathKubernetesDeployment Path = "/apis/apps/v1/namespaces/" + namespace + "/deployments/" + task
*/
func GetPathKubernetesDeployment(orchestrator structs.E2cOrchestrator, namespace string, task string) string {
	url := ""
	if orchestrator == (structs.E2cOrchestrator{}) {
		url = defaultRESTAPIEndPoint + "/apis/apps/v1/namespaces/" + namespace + "/deployments/" + task
		log.Error(pathLOG + "URLs [GetPathKubernetesDeployment] ERROR orchestrator is nil. Returning default orchestrator URL [" + url + "] ...")
	} else if len(namespace) == 0 {
		url = orchestrator.RESTAPIEndPoint + PathKubernetesDeployment + "/" + task
		log.Warn(pathLOG + "URLs [GetPathKubernetesDeployment] Using 'default' namespace. URL: " + url)
	} else {
		url = orchestrator.RESTAPIEndPoint + "/apis/apps/v1/namespaces/" + namespace + "/deployments/" + task
		log.Debug(pathLOG + "URLs [GetPathKubernetesDeployment] URL: " + url)
	}
	return url
}

/*
GetPathKubernetesCreateDeployment Path = "/apis/apps/v1/namespaces/" + namespace + "/deployments"
*/
func GetPathKubernetesCreateDeployment(orchestrator structs.E2cOrchestrator, namespace string) string {
	url := ""
	if orchestrator == (structs.E2cOrchestrator{}) {
		url = defaultRESTAPIEndPoint + "/apis/apps/v1/namespaces/" + namespace + "/deployments"
		log.Error(pathLOG + "URLs [GetPathKubernetesCreateDeployment] ERROR orchestrator is nil. Returning default orchestrator URL [" + url + "] ...")
	} else if len(namespace) == 0 {
		url = orchestrator.RESTAPIEndPoint + PathKubernetesDeployment
		log.Warn(pathLOG + "URLs [GetPathKubernetesCreateDeployment] Using 'default' namespace. URL: " + url)
	} else {
		url = orchestrator.RESTAPIEndPoint + "/apis/apps/v1/namespaces/" + namespace + "/deployments"
		log.Debug(pathLOG + "URLs [GetPathKubernetesCreateDeployment] URL: " + url)
	}
	return url
}

/*
GetPathKubernetesDeleteDeployment Path = "/apis/apps/v1/namespaces/" + namespace + "/deployments/" + task
*/
func GetPathKubernetesDeleteDeployment(orchestrator structs.E2cOrchestrator, namespace string, task string) string {
	url := ""
	if orchestrator == (structs.E2cOrchestrator{}) {
		url = defaultRESTAPIEndPoint + "/apis/apps/v1/namespaces/" + namespace + "/deployments/" + task
		log.Error(pathLOG + "URLs [GetPathKubernetesDeleteDeployment] ERROR orchestrator is nil. Returning default orchestrator URL [" + url + "] ...")
	} else if len(namespace) == 0 {
		url = orchestrator.RESTAPIEndPoint + PathKubernetesDeployment + "/" + task
		log.Warn(pathLOG + "URLs [GetPathKubernetesDeleteDeployment] Using 'default' namespace. URL: " + url)
	} else {
		url = orchestrator.RESTAPIEndPoint + "/apis/apps/v1/namespaces/" + namespace + "/deployments/" + task
		log.Debug(pathLOG + "URLs [GetPathKubernetesDeleteDeployment] URL: " + url)
	}
	return url
}

/*
GetPathKubernetesGetDeployment Path = "/apis/apps/v1/namespaces/" + namespace + "/deployments/" + task
*/
func GetPathKubernetesGetDeployment(orchestrator structs.E2cOrchestrator, namespace string, task string) string {
	url := ""
	if orchestrator == (structs.E2cOrchestrator{}) {
		url = defaultRESTAPIEndPoint + "/apis/apps/v1/namespaces/" + namespace + "/deployments/" + task
		log.Error(pathLOG + "URLs [GetPathKubernetesGetDeployment] ERROR orchestrator is nil. Returning default orchestrator URL [" + url + "] ...")
	} else if len(namespace) == 0 {
		url = orchestrator.RESTAPIEndPoint + PathKubernetesDeployment + "/" + task
		log.Warn(pathLOG + "URLs [GetPathKubernetesGetDeployment] Using 'default' namespace. URL: " + url)
	} else {
		url = orchestrator.RESTAPIEndPoint + "/apis/apps/v1/namespaces/" + namespace + "/deployments/" + task
		log.Debug(pathLOG + "URLs [GetPathKubernetesGetDeployment] URL: " + url)
	}
	return url
}

// Services

/*
GetPathKubernetesCreateService Path = "/api/v1/namespaces/" + namespace + "/services"
*/
func GetPathKubernetesCreateService(orchestrator structs.E2cOrchestrator, namespace string) string {
	url := ""
	if orchestrator == (structs.E2cOrchestrator{}) {
		log.Error(pathLOG + "URLs [GetPathKubernetesCreateService] ERROR orchestrator is nil. Returning default orchestrator URL [" + url + "] ...")
		url = defaultRESTAPIEndPoint + "/api/v1/namespaces/" + namespace + "/services"
	} else if len(namespace) == 0 {
		url = orchestrator.RESTAPIEndPoint + PathKubernetesService
		log.Warn(pathLOG + "URLs [GetPathKubernetesCreateService] Using 'default' namespace. URL: " + url)
	} else {
		url = orchestrator.RESTAPIEndPoint + "/api/v1/namespaces/" + namespace + "/services"
		log.Debug(pathLOG + "URLs [GetPathKubernetesCreateService] URL: " + url)
	}
	return url
}

/*
GetPathKubernetesService Path = "/api/v1/namespaces/" + namespace + "/services/serv-" + task
*/
func GetPathKubernetesService(orchestrator structs.E2cOrchestrator, namespace string, task string) string {
	url := ""
	if orchestrator == (structs.E2cOrchestrator{}) {
		url = defaultRESTAPIEndPoint + "/api/v1/namespaces/" + namespace + "/services/serv-" + task
		log.Error(pathLOG + "URLs [GetPathKubernetesService] ERROR orchestrator is nil. Returning default orchestrator URL [" + url + "] ...")
	} else if len(namespace) == 0 {
		url = orchestrator.RESTAPIEndPoint + PathKubernetesService + "/serv-" + task
		log.Warn(pathLOG + "URLs [GetPathKubernetesService] Using 'default' namespace. URL: " + url)
	} else {
		url = orchestrator.RESTAPIEndPoint + "/api/v1/namespaces/" + namespace + "/services/serv-" + task
		log.Debug(pathLOG + "URLs [GetPathKubernetesService] URL: " + url)
	}
	return url
}

// Scale

/*
GetPathKubernetesScaleDeployment Path = "/apis/apps/v1/namespaces/" + namespace + "/deployments/" + task + "/scale"
*/
func GetPathKubernetesScaleDeployment(orchestrator structs.E2cOrchestrator, namespace string, task string) string {
	url := ""
	if orchestrator == (structs.E2cOrchestrator{}) {
		url = defaultRESTAPIEndPoint + "/apis/apps/v1/namespaces/" + namespace + "/deployments/" + task + "/scale"
		log.Error(pathLOG + "URLs [GetPathKubernetesScaleDeployment] ERROR orchestrator is nil. Returning default orchestrator URL [" + url + "] ...")
	} else if len(namespace) == 0 {
		url = orchestrator.RESTAPIEndPoint + PathKubernetesDeployment + "/" + task + "/scale"
		log.Warn(pathLOG + "URLs [GetPathKubernetesScaleDeployment] Using 'default' namespace. URL: " + url)
	} else {
		url = orchestrator.RESTAPIEndPoint + "/apis/apps/v1/namespaces/" + namespace + "/deployments/" + task + "/scale"
		log.Debug(pathLOG + "URLs [GetPathKubernetesScaleDeployment] URL: " + url)
	}
	return url
}


/*
GetPathKubernetesVScaleDeployment Path = "/apis/apps/v1/namespaces/" + namespace + "/deployments/" + task
*/
func GetPathKubernetesVScaleDeployment(orchestrator structs.E2cOrchestrator, namespace string, task string) string {
	url := ""
	if orchestrator == (structs.E2cOrchestrator{}) {
		url = defaultRESTAPIEndPoint + "/apis/apps/v1/namespaces/" + namespace + "/deployments/" + task
		log.Error(pathLOG + "URLs [GetPathKubernetesVScaleDeployment] ERROR orchestrator is nil. Returning default orchestrator URL [" + url + "] ...")
	} else if len(namespace) == 0 {
		url = orchestrator.RESTAPIEndPoint + PathKubernetesDeployment + "/" + task
		log.Warn(pathLOG + "URLs [GetPathKubernetesVScaleDeployment] Using 'default' namespace. URL: " + url)
	} else {
		url = orchestrator.RESTAPIEndPoint + "/apis/apps/v1/namespaces/" + namespace + "/deployments/" + task
		log.Debug(pathLOG + "URLs [GetPathKubernetesVScaleDeployment] URL: " + url)
	}
	return url
}

// Pods

/*
GetPathKubernetesPodsApp Path = "/api/v1/namespaces/" + namespace + "/pods?labelSelector=app=" + task
*/
func GetPathKubernetesPodsApp(orchestrator structs.E2cOrchestrator, namespace string, task string) string {
	url := ""
	if orchestrator == (structs.E2cOrchestrator{}) {
		url = defaultRESTAPIEndPoint + "/api/v1/namespaces/" + namespace + "/pods?labelSelector=app=" + task
		log.Error(pathLOG + "URLs [GetPathKubernetesPodsApp] ERROR orchestrator is nil. Returning default orchestrator URL [" + url + "] ...")
	} else if len(namespace) == 0 {
		// 'default' namespace
		url = orchestrator.RESTAPIEndPoint + PathKubernetesPodsApp + "/" + task
		log.Warn(pathLOG + "URLs [GetPathKubernetesPodsApp] Using 'default' namespace. URL: " + url)
	} else {
		url = orchestrator.RESTAPIEndPoint + "/api/v1/namespaces/" + namespace + "/pods?labelSelector=app=" + task
		log.Debug(pathLOG + "URLs [GetPathKubernetesPodsApp] URL: " + url)
	}
	return url
}

/*
GetPathKubernetesPod Path = "/api/v1/namespaces/" + namespace + "/pods/" + podName
*/
func GetPathKubernetesPod(orchestrator structs.E2cOrchestrator, namespace string, podName string) string {
	url := ""
	if orchestrator == (structs.E2cOrchestrator{}) {
		url = defaultRESTAPIEndPoint + "/api/v1/namespaces/" + namespace + "/pods/" + podName
		log.Error(pathLOG + "URLs [GetPathKubernetesPod] ERROR orchestrator is nil. Returning default orchestrator URL [" + url + "] ...")
	} else if len(namespace) == 0 {
		url = orchestrator.RESTAPIEndPoint + PathKubernetesPod + "/" + podName
		log.Warn(pathLOG + "URLs [GetPathKubernetesPod] Using 'default' namespace. URL: " + url)
	} else {
		url = orchestrator.RESTAPIEndPoint + "/api/v1/namespaces/" + namespace + "/pods/" + podName
		log.Debug(pathLOG + "URLs [GetPathKubernetesPod] URL: " + url)
	}
	return url
}

///////////////////////////////////////////////////////////////////////////////
// OPENSHIFT

/*
GetPathOpenshiftRoute Path = "/apis/route.openshift.io/v1/namespaces/" + namespace + "/routes/route-" + task
PathOpenshiftRoute = "/apis/route.openshift.io/v1/namespaces/default/routes/route-"
*/
func GetPathOpenshiftRoute(orchestrator structs.E2cOrchestrator, namespace string, task string) string {
	url := ""
	if orchestrator == (structs.E2cOrchestrator{}) {
		url = defaultRESTAPIEndPoint + "/apis/route.openshift.io/v1/namespaces/" + namespace + "/routes/route-" + task
		log.Error(pathLOG + "URLs [GetPathOpenshiftRoute] ERROR orchestrator is nil. Returning default orchestrator URL [" + url + "] ...")
	} else if len(namespace) == 0 {
		url = orchestrator.RESTAPIEndPoint + PathOpenshiftRoute + task
		log.Warn(pathLOG + "URLs [GetPathOpenshiftRoute] Using 'default' namespace. URL: " + url)
	} else {
		url = orchestrator.RESTAPIEndPoint + "/apis/route.openshift.io/v1/namespaces/" + namespace + "/routes/route-" + task
		log.Debug(pathLOG + "URLs [GetPathOpenshiftRoute] URL: " + url)
	}
	return url
}

/*
GetPathOpenshiftRoutes Path = "/apis/route.openshift.io/v1/namespaces/"+namespace+"/routes"
PathOpenshiftRoutes "/apis/route.openshift.io/v1/namespaces/"+namespace+"/routes"
*/
func GetPathOpenshiftRoutes(orchestrator structs.E2cOrchestrator, namespace string) string {
	url := ""
	if orchestrator == (structs.E2cOrchestrator{}) {
		url = defaultRESTAPIEndPoint + "/apis/route.openshift.io/v1/namespaces/" + namespace + "/routes"
		log.Error(pathLOG + "URLs [GetPathOpenshiftRoutes] ERROR orchestrator is nil. Returning default orchestrator URL [" + url + "] ...")
	} else if len(namespace) == 0 {
		url = orchestrator.RESTAPIEndPoint + PathOpenshiftRoutes
		log.Warn(pathLOG + "URLs [GetPathOpenshiftRoutes] Using 'default' namespace. URL: " + url)
	} else {
		url = orchestrator.RESTAPIEndPoint + "/apis/route.openshift.io/v1/namespaces/" + namespace + "/routes"
		log.Debug(pathLOG + "URLs [GetPathOpenshiftRoutes] URL: " + url)
	}
	return url
}

///////////////////////////////////////////////////////////////////////////////
// SOE

/*
GetPathSOENetworkInstance Path = "/network_service_instance"
*/
func GetPathSOENetworkInstance(orchestrator structs.E2cOrchestrator) string {
	url := ""
	if orchestrator == (structs.E2cOrchestrator{}) {
		url = defaultRESTAPIEndPoint + "/network_service_instance"
		log.Error(pathLOG + "URLs [GetPathSOENetworkInstance] ERROR orchestrator is nil. Returning default orchestrator URL [" + url + "] ...")
	} else {
		url = orchestrator.RESTAPIEndPoint + "/network_service_instance"
		log.Debug(pathLOG + "URLs [GetPathSOENetworkInstance] URL: " + url)
	}
	return url
}

/*
GetPathSOEComputeChunk Path = "/compute_chunk"
*/
func GetPathSOEComputeChunk(orchestrator structs.E2cOrchestrator) string {
	url := ""
	if orchestrator == (structs.E2cOrchestrator{}) {
		url = defaultRESTAPIEndPoint + "/compute_chunk"
		log.Error(pathLOG + "URLs [GetPathSOEComputeChunk] ERROR orchestrator is nil. Returning default orchestrator URL [" + url + "] ...")
	} else {
		url = orchestrator.RESTAPIEndPoint + "/compute_chunk"
		log.Debug(pathLOG + "URLs [GetPathSOEComputeChunk] URL: " + url)
	}
	return url
}

/*
GetPathSOESlic3 Path = "/slic3"
*/
func GetPathSOESlic3(orchestrator structs.E2cOrchestrator) string {
	url := ""
	if orchestrator == (structs.E2cOrchestrator{}) {
		url = defaultRESTAPIEndPoint + "/slic3"
		log.Error(pathLOG + "URLs [GetPathSOESlic3] ERROR orchestrator is nil. Returning default orchestrator URL [" + url + "] ...")
	} else {
		url = orchestrator.RESTAPIEndPoint + "/slic3"
		log.Debug(pathLOG + "URLs [GetPathSOESlic3] URL: " + url)
	}
	return url
}

///////////////////////////////////////////////////////////////////////////////
// Simple Path

/*
GetPath Path = "/slic3"
*/
func GetPath(orchestrator structs.E2cOrchestrator) string {
	url := ""
	if orchestrator == (structs.E2cOrchestrator{}) {
		url = defaultRESTAPIEndPoint
		log.Error(pathLOG + "URLs [GetPath] ERROR orchestrator is nil. Returning default orchestrator URL [" + url + "] ...")
	} else {
		url = orchestrator.RESTAPIEndPoint
		log.Debug(pathLOG + "URLs [GetPath] URL: " + url)
	}

	if strings.HasSuffix(url, "/") {
		return url
	} else {
		return url + "/"
	}
}