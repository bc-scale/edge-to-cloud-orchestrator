#
#   Env vars for testing with docker
#   e.g.: docker run --rm --name e2co-app --env-file env-test.txt -v "$HOME/tmp/mongodb:/opt/e2co/data" -v "$HOME/tmp/kafka/keystore/localdev:/var/kafka_keystore" -v "$HOME/tmp/kafka/truststore/localdev:/var/kafka_truststore" -p 31000:8333 e2co
#
#SLALiteEndPoint=http://host.docker.internal:32000
#KafkaEndPoint=host.docker.internal:9094
#KafkaTopic=sla_violation
E2CO_SLA_ENDPOINT=http://localhost:32000
DatabaseRepository=/opt/e2co/data/e2co.db
E2CO_WARMING_TIME=180
E2CO_CONFIGSVC_ENDPOINT="http://localhost:8000/api/"
E2CO_CONFIGSVC_CREDENTIALS=""url:http://localhost:8000/api/authenticate,username:api,password:apiH2020""
E2CO_KAFKA_CLIENT=kafka2
E2CO_KAFKA_ENDPOINT=host.docker.internal:9094
E2CO_KAFKA_NOTIFIER_TOPIC=sla_violation
E2CO_KAFKA_AGREEMENT_TOPIC=configuration
E2CO_KAFKA_KEYSTORE_PASSWORD=password
E2CO_KAFKA_KEY_PASSWORD=password
E2CO_KAFKA_TRUSTSTORE_PASSWORD=password
E2CO_KAFKA_KEYSTORE_LOCATION=/var/kafka_keystore/kafka.client.keystore.p12
E2CO_KAFKA_TRUSTSTORE_LOCATION=/var/kafka_truststore/kafka.client.truststore.pem
E2CO_KAFKA_SOE_TOPIC=soe_provisioning
