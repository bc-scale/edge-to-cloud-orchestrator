//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 30 Mar 2021
// Updated on 04 Aug 2021
//
// @author: ATOS
//
package swarm

import (
	"atos.pledger/e2c-orchestrator/data/structs"
	log "atos.pledger/e2c-orchestrator/common/logs"
)

// path used in logs
const pathLOG string = "E2CO > Orchestrator > Deployment-Engine > Docker-Swarm "

/*
Adapter Adapter
*/
type Adapter struct{}

/*
DeployTask Deploy a task
*/
func (a Adapter) DeployTask(app *structs.E2coSubApp, orch structs.E2cOrchestrator) (string, error) {
	log.Warn(pathLOG + "[DeployTask] Function Not Implemented")
	return "", nil
}

/*
RemoveTask Remove a task
*/
func (a Adapter) RemoveTask(app *structs.E2coSubApp, orch structs.E2cOrchestrator) (string, error) {
	log.Warn(pathLOG + "[RemoveTask] Function Not Implemented")
	return "", nil
}

/*
GetTask Gets a task information (cluster information)
*/
func (a Adapter) GetTask(app structs.E2coSubApp, orch structs.E2cOrchestrator) (structs.E2coTaskStatusInfo, error) {
	log.Warn(pathLOG + "[GetTask] Function Not Implemented")
	return structs.E2coTaskStatusInfo{}, nil
}