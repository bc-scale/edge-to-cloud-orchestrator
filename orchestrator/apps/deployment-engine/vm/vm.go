//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 04 Aug 2021
//
// @author: ATOS
//
package vm

import (
	"strconv"

	"atos.pledger/e2c-orchestrator/common"
	"atos.pledger/e2c-orchestrator/data/structs"
	"atos.pledger/e2c-orchestrator/common/http"
	"atos.pledger/e2c-orchestrator/common/cfg"
	log "atos.pledger/e2c-orchestrator/common/logs"
)

// path used in logs
const pathLOG string = "E2CO > Orchestrator > Deployment-Engine > VM "

/*
Adapter Adapter
*/
type Adapter struct{}

/*
DeployTask Deploy a task
*/
func (a Adapter) DeployTask(app *structs.E2coSubApp, orch structs.E2cOrchestrator) (string, error) {
	// Auth Token needed?
	sec := common.AuthTokenNeeded(orch)

	// CALL to VM API to launch a new deployment
	log.Info(pathLOG + "[DeployTask] Starting VM ...")
	status, _, err := http.PostRawData(
		cfg.GetPath(orch) + "start",
		sec,
		orch.ConnectionToken,
		"")
	/*status, _, err := http.Get(
		cfg.GetPath(orch) + "start",
		sec,
		orch.ConnectionToken)*/
	if err != nil {
		log.Error(pathLOG+"[DeployTask] ERROR", err)
		return "", err
	}
	log.Info(pathLOG + "[DeployTask] RESPONSE: OK")

	return strconv.Itoa(status), nil
}

/*
RemoveTask Remove a task
*/
func (a Adapter) RemoveTask(app *structs.E2coSubApp, orch structs.E2cOrchestrator) (string, error) {
	// Auth Token needed?
	sec := common.AuthTokenNeeded(orch)

	// CALL to VM API to stop a deployment
	log.Info(pathLOG + "[RemoveTask] Starting VM ...")
	status, _, err := http.PostRawData(
		cfg.GetPath(orch) + "stop",
		sec,
		orch.ConnectionToken,
		"")
	/*status, _, err := http.Get(
		cfg.GetPath(orch) + "stop",
		sec,
		orch.ConnectionToken)*/
	if err != nil {
		log.Error(pathLOG+"[RemoveTask] ERROR", err)
		return "", err
	}
	log.Info(pathLOG + "[RemoveTask] RESPONSE: OK")

	return strconv.Itoa(status), nil
}

/*
GetTask Gets a task information (cluster information)
*/
func (a Adapter) GetTask(app structs.E2coSubApp, orch structs.E2cOrchestrator) (structs.E2coTaskStatusInfo, error) {
	log.Warn(pathLOG + "[GetTask] Function Not Implemented")
	return structs.E2coTaskStatusInfo{}, nil
}