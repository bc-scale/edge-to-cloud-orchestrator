//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 07 Apr 2021
//
// @author: ATOS
//
package deploy

import (
	"strconv"

	"atos.pledger/e2c-orchestrator/common"
	"atos.pledger/e2c-orchestrator/common/cfg"
	"atos.pledger/e2c-orchestrator/common/http"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
)

// path used in logs
const pathLOG string = "E2CO > Orchestrator > Deployment-Engine > K8s > Deploy "

/* K8S DEPLOYMENT example:

{
  "apiVersion": "apps/v1",
  "kind": "Deployment",
  "metadata": {
    "name": "rss-site",
    "labels": {
      "app": "web"
    }
  },
  "spec": {
    "replicas": 2,
    "selector": {
      "matchLabels": {
        "app": "web"
      }
    },
    "template": {
      "metadata": {
        "labels": {
          "app": "web"
        }
      },
      "spec": {
        "containers": [
          {
            "name": "front-end",
            "image": "nginx",
            "ports": [
              {
                "containerPort": 80
              }
            ]
          },
          {
            "name": "rss-reader",
            "image": "nickchase/rss-php-nginx:v1",
            "ports": [
              {
                "containerPort": 88
              }
            ]
          }
        ]
      }
    }
  }
}
*/

/* 
NewDeploymentStruct creates a new K8s Deployment json
*/
func NewDeploymentStruct(app structs.E2coSubApp, namespace string, replicas int) *structs.K8sDeployment {
	var jsonDeployment *structs.K8sDeployment
	jsonDeployment = new(structs.K8sDeployment)

	jsonDeployment.APIVersion = "apps/v1"
	jsonDeployment.Kind = "Deployment"
	jsonDeployment.Metadata.Name = app.ID
	jsonDeployment.Metadata.Namespace = namespace

	// replicas
	if app.Replicas > 1 {
		jsonDeployment.Spec.Replicas = app.Replicas
	} else {
		jsonDeployment.Spec.Replicas = replicas
	}

	jsonDeployment.Spec.RevisionHistoryLimit = 10

	// labels
	jsonDeployment.Metadata.Labels = map[string]interface{}{
		"app": app.ID,
	}
	jsonDeployment.Spec.Selector.MatchLabels = map[string]interface{}{
		"app": app.ID,
	}
	jsonDeployment.Spec.Template.Metadata.Labels = map[string]interface{}{
		"app": app.ID,
	}

	// metadata: labels
	totalLabels := len(app.Labels)
	if totalLabels > 0 {
		for k := 0; k < totalLabels; k++ {
			jsonDeployment.Spec.Template.Metadata.Labels[app.Labels[k].Key] = app.Labels[k].Value
			jsonDeployment.Spec.Selector.MatchLabels[app.Labels[k].Key] = app.Labels[k].Value
		}
	}

	// containers
	totalContainers := len(app.Containers)
	jsonDeployment.Spec.Template.Spec.Containers = make([]structs.K8sDeploymentContainer, totalContainers)
	for i := 0; i < totalContainers; i++ {
		jsonDeployment.Spec.Template.Spec.Containers[i].Image = app.Containers[i].Image
		if len(app.Containers[i].ImagePullPolicy) > 0 {
			jsonDeployment.Spec.Template.Spec.Containers[i].ImagePullPolicy = app.Containers[i].ImagePullPolicy
		} else {
			jsonDeployment.Spec.Template.Spec.Containers[i].ImagePullPolicy = "Always"
		}
		
		//jsonDeployment.Spec.Template.Spec.Containers[i].ImagePullSecrets = app.Containers[i].ImagePullSecrets 
		// TODO change this part -> ImagePullSecrets in all structs
		jsonDeployment.Spec.Template.Spec.ImagePullSecrets = app.Containers[i].ImagePullSecrets
		if len(app.Containers[i].Name) == 0 {
			jsonDeployment.Spec.Template.Spec.Containers[i].Name = app.Name + strconv.Itoa(i)
		} else {
			jsonDeployment.Spec.Template.Spec.Containers[i].Name = app.Containers[i].Name
		}
		
		// metadata: labels
		/*totalLabels := len(app.Containers[i].Labels)
		if totalLabels > 0 {
			m := jsonDeployment.Spec.Template.Metadata.Labels
			m2 := jsonDeployment.Spec.Selector.MatchLabels
			for k := 0; k < totalLabels; k++ {
				m[app.Containers[i].Labels[k].Key] = app.Containers[i].Labels[k].Value
				m2[app.Containers[i].Labels[k].Key] = app.Containers[i].Labels[k].Value
			}
		}*/

		// ports
		totalPorts := len(app.Containers[i].Ports)
		jsonDeployment.Spec.Template.Spec.Containers[i].Ports = make([]structs.K8sDeploymentContainerPorts, totalPorts)
		for j := 0; j < totalPorts; j++ {
			jsonDeployment.Spec.Template.Spec.Containers[i].Ports[j].ContainerPort = app.Containers[i].Ports[j].ContainerPort
		}

		// env
		totalEnvs := len(app.Containers[i].Environment)
		if totalEnvs > 0 {
			jsonDeployment.Spec.Template.Spec.Containers[i].Env = make([]structs.K8sDeploymentContainerEnv, totalEnvs)
			for j := 0; j < totalEnvs; j++ {
				jsonDeployment.Spec.Template.Spec.Containers[i].Env[j].Name = app.Containers[i].Environment[j].Name
				jsonDeployment.Spec.Template.Spec.Containers[i].Env[j].Value = app.Containers[i].Environment[j].Value
			}
		}

		// resources
		if app.Containers[i].Hw != (structs.E2coAppkHwProps{}) {
			jsonDeployment.Spec.Template.Spec.Containers[i].Resources.Requests.Memory = app.Containers[i].Hw.Memory
			jsonDeployment.Spec.Template.Spec.Containers[i].Resources.Requests.Cpu = app.Containers[i].Hw.Cpu

			jsonDeployment.Spec.Template.Spec.Containers[i].Resources.Limits.Memory = app.Containers[i].Hw.Memory
			jsonDeployment.Spec.Template.Spec.Containers[i].Resources.Limits.Cpu = app.Containers[i].Hw.Cpu
		}

		// command & args
		if len(app.Containers[i].Command) >= 1 {
			jsonDeployment.Spec.Template.Spec.Containers[i].Command = app.Containers[i].Command
		}

		if len(app.Containers[i].Args) >= 1 {
			jsonDeployment.Spec.Template.Spec.Containers[i].Args = app.Containers[i].Args
		}
	}

	// spec.template.spec.nodeSelector
	if len(app.NodeSelector) > 0 {
		jsonDeployment.Spec.Template.Spec.NodeSelector = make(map[string]interface{}, len(app.NodeSelector))
		for key, value := range app.NodeSelector {
			jsonDeployment.Spec.Template.Spec.NodeSelector[key] = value
		}
	}

	log.Debug(pathLOG + "[newDeploymentStruct] Returning jsonDeployment struct ...")

	return jsonDeployment
}

/* 
CreateK8sDeployment creates the deployment json used by K8s; call to Kubernetes API to create a deployment
*/
func CreateK8sDeployment(app *structs.E2coSubApp, orch structs.E2cOrchestrator, sec bool) (string, error) {
	log.Info(pathLOG + "[CreateK8sDeployment] Generating 'deployment' json ...")

	namespace := common.GetAppNamespace(*app, orch)
	k8sDepl := NewDeploymentStruct(*app, namespace, 1) // returns *structs.K8sDeployment

	strTxt, _ := common.StructToString(*k8sDepl)
	log.Info(pathLOG + "[CreateK8sDeployment] [" + strTxt + "]")

	// CALL to Kubernetes API to launch a new deployment
	log.Info(pathLOG + "[CreateK8sDeployment] Creating a new deployment in K8s cluster ...")
	status, _, err := http.PostStruct(
		cfg.GetPathKubernetesCreateDeployment(orch, namespace),
		sec,
		orch.ConnectionToken,
		k8sDepl)
	if err != nil {
		log.Error(pathLOG+"[CreateK8sDeployment] ERROR", err)
		return "", err
	}
	log.Info(pathLOG + "[CreateK8sDeployment] RESPONSE: OK")

	return strconv.Itoa(status), nil
}