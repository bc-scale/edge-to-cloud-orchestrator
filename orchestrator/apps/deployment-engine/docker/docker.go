//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 07 Apr 2021
// Updated on 04 Aug 2021
//
// @author: ATOS
//
package docker

import (
	"errors"
	"context"

	"atos.pledger/e2c-orchestrator/common"
	"atos.pledger/e2c-orchestrator/common/globals"
	"atos.pledger/e2c-orchestrator/data/structs"
	log "atos.pledger/e2c-orchestrator/common/logs"
	
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
)


// path used in logs
const pathLOG string = "E2CO > Orchestrator > Deployment-Engine > Docker "

/*
Adapter Adapter
*/
type Adapter struct{}

/*
DeploymentErrorResponse Error Response from call to Docker API
*/
type DeploymentErrorResponse struct {
	Code      int 		`json:"code,omitempty"`
	Message   string 	`json:"message,omitempty"`
}

/*
DeploymentResponse Response from call to Docker API
*/
type DeploymentResponse struct {
	State     		DeploymentStatusResponse 	`json:"state,omitempty"`
	ID        		string 						`json:"Id,omitempty"`
	Created   		string 						`json:"created,omitempty"`
	RestartCount    int							`json:"restartCount,omitempty"`
}

/*
DeploymentStatusResponse Status struct
*/
type DeploymentStatusResponse struct {
	Status    	string 	`json:"status,omitempty"`
	Running     bool 	`json:"running,omitempty"`
	Paused   	bool 	`json:"paused,omitempty"`
	Restarting  bool 	`json:"restarting,omitempty"`
	Dead   		bool 	`json:"dead,omitempty"`
	ExitCode   	int 	`json:"exitCode,omitempty"`
	Error   	string 	`json:"error,omitempty"`
	StartedAt   string 	`json:"startedAt,omitempty"`
	FinishedAt  string 	`json:"finishedAt,omitempty"`
}

///////////////////////////////////////////////////////////////////////////////

// connect: connects with Docker exposed socket / api
func connect(orch structs.E2cOrchestrator) (*client.Client, error) {
	log.Debug(pathLOG + "[connect] Connecting to docker endpoint...")
	cli, err := client.NewClientWithOpts(client.WithHost(orch.RESTAPIEndPoint), client.WithAPIVersionNegotiation())
	if err != nil {
		return nil, errors.New(" ERROR (1), could not create docker client handler: " + err.Error())
	}

	log.Debug(pathLOG + "[connect] Connection established.")
	return cli, err
}


// getContainerStatus: get the status of a container
func getContainerStatus(app structs.E2coSubApp, orch structs.E2cOrchestrator) (DeploymentResponse, error) {
	log.Debug(pathLOG + "[getContainerStatus] Getting container info...")
	
	// connection
	cli, err := connect(orch)
	if err != nil {
		return DeploymentResponse{}, errors.New(" ERROR (1) connecting to client: " + err.Error())
	}

	// info
	ctx := context.Background()
	info, err := cli.ContainerInspect(ctx, app.AssignedID)
	if err != nil {
		return DeploymentResponse{}, errors.New(" ERROR (2) getting info from docker container: " + err.Error())
	}

	// info to DeploymentResponse
	var resp DeploymentResponse
	resp.ID = info.ID
	resp.Created = info.Created
	resp.RestartCount = info.RestartCount
	resp.State.Status = info.State.Status
	resp.State.Running = info.State.Running
	resp.State.Paused = info.State.Paused
	resp.State.Restarting = info.State.Restarting
	resp.State.Dead = info.State.Dead
	resp.State.ExitCode = info.State.ExitCode
	resp.State.Error = info.State.Error
	resp.State.StartedAt = info.State.StartedAt
	resp.State.FinishedAt = info.State.FinishedAt

	return resp, nil
}

///////////////////////////////////////////////////////////////////////////////

/*
GetTask Gets a task information (cluster information)
*/
func (a Adapter) GetTask(app structs.E2coSubApp, orch structs.E2cOrchestrator) (structs.E2coTaskStatusInfo, error) {
	// CALL to Docker API to get deployment
	log.Debug(pathLOG + "[GetTask] Getting deployment from Docker ...")

	// Auth Token needed?
	_ = common.AuthTokenNeeded(orch) // TODO "secure" docker

	// structs.E2coTaskStatusInfo
	status := structs.E2coTaskStatusInfo{}
	status.Status = globals.DEPLOYMENT_UNDEFINED
	status.Type = "docker_container_id"

	// call to Docker API
	obj, err := getContainerStatus(app, orch)
	if err != nil {
		return status, errors.New("Error getting status from container: " + err.Error())
	} else {
		// DEBUG Container INFO
		strObj, err := common.StructToString(obj)
		if err == nil {
			log.Debug(pathLOG + "[GetTask] Container INFO: " + strObj)
		}

		log.Debug(pathLOG + "[GetTask] Task status: " + obj.State.Status)
		if obj.State.Running {
			status.Status = globals.DEPLOYMENT_DEPLOYED
			status.Obj = obj
			status.IDObj = obj.ID
			status.Message = "docker application deployed with ID " + obj.ID
		} else {
			status.Status = globals.DEPLOYMENT_NOT_READY
			status.Message = "docker application not ready "
			status.Obj = obj
		}
		
		return status, nil
	}
}

/*
DeployTask Deploy a new container application (Docker: deployment...)
==> https://godoc.org/github.com/docker/docker/client
*/
func (a Adapter) DeployTask(subapp *structs.E2coSubApp, orch structs.E2cOrchestrator) (string, error) {
	log.Debug(pathLOG + "[DeployApp] Deploying docker application [name="+subapp.Name+"] ...")

	cli, err := connect(orch)	// connection to docker socket
	if err != nil {
		return "error", errors.New(" ERROR (1) connecting to client: " + err.Error())
	}

	// create and start application container
	idContainer, err := createStartContainer(*cli, subapp, orch)
	if err != nil {
		return "error", errors.New(" ERROR (2) creating container [name="+subapp.Name+"]: " + err.Error())
	}
	subapp.AssignedID = idContainer

	subapp.Info.Message = "docker application deployed with ID " + idContainer
	subapp.Info.Type = "docker_container_id"
	subapp.Info.IDObj = idContainer

	log.Debug(pathLOG + " [DeployApp] Docker application deployed [name="+subapp.Name+"]: " + subapp.AssignedID)

	return "ok", nil
}

/*
RemoveTask Remove the container application
*/
func (a Adapter) RemoveTask(task *structs.E2coSubApp, orch structs.E2cOrchestrator) (string, error) {
	log.Debug(pathLOG + "[RemoveApp] Terminating docker application [" + task.AssignedID + "] ...")

	cli, err := connect(orch)	// connection to docker socket
	if err != nil {
		return "error", errors.New(" ERROR (1) connecting to client: " + err.Error())
	}

	// stops and remove
	ctx := context.Background()

	// func (cli *Client) ContainerStop(ctx context.Context, containerID string, timeout *time.Duration) error
	// func (cli *Client) ContainerRemove(ctx context.Context, containerID string, options types.ContainerRemoveOptions) error
	err = cli.ContainerRemove(ctx,
		task.AssignedID,
		types.ContainerRemoveOptions{
			Force: true,
		})
	if err != nil {
		if client.IsErrNotFound(err) {
			log.Warn(pathLOG + "[RemoveApp] Docker application not found!")
			return "ok", nil
		}
		if client.IsErrConnectionFailed(err) {
			log.Warn(pathLOG + "[RemoveApp] Docker Engine not available!")
			return "ok", nil
		}
		return "error", errors.New(" ERROR (2) removing container: " + err.Error())
	}

	log.Debug(pathLOG + "[RemoveApp] Docker application [" + task.AssignedID + "] was removed ...")
	return "ok", nil
}