//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 23 Mar 2021
// Updated on 18 Mar 2021
//
// @author: ATOS
//
package apps

import (
	deploymentengine "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine"
	connectors "atos.pledger/e2c-orchestrator/connectors"
	log "atos.pledger/e2c-orchestrator/common/logs"
	structs "atos.pledger/e2c-orchestrator/data/structs"
)


/*
terminationProcess Termination background processes
*/
func terminationProcess(app structs.E2coSubApp, orch structs.E2cOrchestrator, a deploymentengine.Adapter) {
	go func() {
		log.Info(pathLOG + "[terminationProcess] Starting background process ...")

		// remove application
		_, err := a.RemoveTask(&app, orch)
		if err != nil {
			log.Error(pathLOG+"[terminationProcess] Error deleting application: ", err)
		} else {
			log.Info(pathLOG + "[terminationProcess] Application has been removed. Deleting information from database ...")
	
			// call to CONNECTORS
			if len(app.Qos) > 0 {
				log.Info(pathLOG+"[terminationProcess] Calling the SLA-Manager to terminate the SLA ...")
				connectors.TerminateSLA(app.ID)
			}
		}

		log.Info(pathLOG + "[terminationProcess] Finalizing background process ...")
	}() 
}