//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 05 Aug 2021
//
// @author: ATOS
//
package eventhandler

import (
	"fmt"
	"errors"
	"strconv"
	"strings"
	"time"
	"encoding/json"

	"atos.pledger/e2c-orchestrator/data/db"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
	orchestrator "atos.pledger/e2c-orchestrator/orchestrator/apps"
	parsers "atos.pledger/e2c-orchestrator/data/parsers/kafka-dss"
	"atos.pledger/e2c-orchestrator/common"
	"atos.pledger/e2c-orchestrator/common/globals"
)

/*
kafkaDeployEventsLoop Kafka events handler for Deployment changes (Infrastructure and apps)
*/
func kafkaDeployEventsLoop() {
	log.Info(pathLOG + "[kafkaDeployEventsLoop] Starting to listen topic " + KAFKA_DEPLOY_TOPIC + " ...")

	for {
		message, err := KafkaAdapter.CheckForMessages(KAFKA_DEPLOY_TOPIC)
		if err != nil {
			log.Error(pathLOG+"[kafkaDeployEventsLoop] Error reading kafka event ...", err)
			sendError("kafkaDeployEventsLoop", "CheckForMessages", err)
			continue
		}

		// Message processing ...
		log.Info(pathLOG+">>>>>> [kafkaDeployEventsLoop] Event received from kafka <<<<<< \n", message)

		// Message is a JSON with information of infrastructure or app config
		// We expect something like this: {'id': 1, 'entity': 'infrastructure or app', 'operation': 'update'}
		// And we need to call ConfService REST API /api/infrastructure/{id} or (/apps/{id} and /api/service/{id})
		var eventObj interface{}

		if err = common.ParseJSONObj(message, &eventObj); err != nil {
			sendError("kafkaDeployEventsLoop", "Unmarshal", err)
			continue
		}

		eventInfoMap := eventObj.(map[string]interface{})
		if eventInfoMap["id"] == nil || eventInfoMap["entity"] == nil || eventInfoMap["operation"] == nil {
			log.Error(pathLOG + "'id' / 'entity' / 'operation' not included in JSON object")
			continue
		}

		// ID
		entityIDn := eventInfoMap["id"].(float64)
		if entityIDn == 0 {
			continue
		}
		entityID := fmt.Sprintf("%d", uint64(entityIDn))
		if entityID == "" {
			log.Warn(pathLOG + "'id' field (entity identifier) is empty")
			continue
		}
		// Entity
		entity := eventInfoMap["entity"].(string)
		if entity == "" || (entity != "service" && entity != "infrastructure") {
			continue
		}
		// Operation
		operation := eventInfoMap["operation"].(string)
		if operation == "" {
			continue
		}

		switch entity {
			case "infrastructure": // SOE / network
				switch operation { 
					case ACTION_PROVISION:
						if err = processCreateComputeChunkAndSlice(entityID, eventInfoMap); err != nil {
							sendError("kafkaDeployEventsLoop", "processCreateComputeChunk", err)
						}
					default:
						continue
				}
			case "service": // app or microservice
				switch operation {
					case ACTION_START:
						if err = processAppStart(entityID, eventInfoMap); err != nil {
							sendError("kafkaDeployEventsLoop", "processAppStart", err)
						} else {
							// Send Message to Kafka
							for i := 0; i < 10; i++ {
								time.Sleep(30 * time.Second)

								log.Info(pathLOG + "[kafkaDeployEventsLoop] Checking application [" + entityID + "] status [" + strconv.Itoa(i) + "] ...")
								e2coApp, err := orchestrator.GetE2coApplication(entityID)
								if err != nil {
									log.Error(pathLOG + "[kafkaDeployEventsLoop] Application [" + entityID + "] info not found!")
								} else if e2coApp.Status == globals.DEPLOYMENT_DEPLOYED {
									log.Info(pathLOG + "[kafkaDeployEventsLoop] Sending application [" + entityID + "] status / info [" + strconv.Itoa(i) + "] to Kafka ...")
									
									content, err := json.Marshal(e2coApp)
									if err == nil {
										sendMessage(KAFKA_RESPONSE_DEPLOY_TOPIC, string(content))
									}
									
									break
								} else {
									log.Warn(pathLOG + "[kafkaDeployEventsLoop] Application [" + entityID + "] status: " + e2coApp.Status)
								}
							}
						}
					case ACTION_STOP:
						if err = processAppStop(entityID); err != nil {
							sendError("kafkaDeployEventsLoop", "processAppStart", err)
						}
					case ACTION_MIGRATE:
						if err = processAppPlacement(entityID, eventInfoMap); err != nil {
							sendError("kafkaDeployEventsLoop", "processAppPlacement", err)
						}
					case ACTION_SCALE_OUT, ACTION_SCALE_IN:
						if err = processAppScaleInOut(entityID, eventInfoMap, operation); err != nil {
							sendError("kafkaDeployEventsLoop", "processAppScaleInOut", err)
						}
					case ACTION_REDEPLOY:
						// https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/deployment-v1/#update-replace-the-specified-deployment
						// ==> PUT /apis/apps/v1/namespaces/{namespace}/deployments/{name
						processAppRedeploy(entityID)
					case ACTION_SCALE_UP,ACTION_SCALE_DOWN:
						if err = processAppScaleUpDown(entityID, eventInfoMap, operation); err != nil {
							sendError("kafkaDeployEventsLoop", "processAppScaleUpDown", err)
						}
					default:
						continue
					}
			default:
				continue
			}
	} // endless for
} // kafkaDeployEventsLoop()

// processAppRedeploy Process App redeploy
func processAppRedeploy(id string) {
	if id == "" {
		log.Warn(pathLOG + "[processAppRedeploy] 'id' is empty")
		return
	}

	// Call E2CO API / methods for app info
	e2coApp, err := orchestrator.GetE2coApplication(id) // GetE2coApplication(idTask string) (structs.E2coApplication, error)
	if err != nil { // error or app not found
		log.Error(pathLOG + "[processAppRedeploy] App info not found!")
		return
	}

	// App found
	var operacion structs.TaskOperation
	operacion.Operation = "update"
	operacion.Payload = e2coApp
	orchestrator.UpdateE2coApplication(&e2coApp, operacion) // UpdateE2coApplication(app *structs.E2coApplication, operation structs.TaskOperation) error
}

// processAppPlacement Process App placement (migration)
func processAppPlacement(id string, eventInfoMap map[string]interface{}) error {
	log.Info(pathLOG + "[processAppPlacement] Migrating application ...")

	if id == "" {
		log.Warn(pathLOG + "[processAppPlacement] 'id' field is empty")
		return errors.New("application 'id' is empty")
	} else if eventInfoMap["target_infra_id"] == nil {
		log.Error(pathLOG + "[processAppPlacement] 'target_infra_id' field is empty")
		return errors.New("'target_infra_id' field is empty")
	}

	// Call E2CO API / methods for app info
	e2coApp, err := orchestrator.GetE2coApplication(id) // GetE2coApplication(idTask string) (structs.E2coApplication, error)
	if err != nil { // error or app not found
		log.Error(pathLOG + "[processAppPlacement] App info not found!")
		return errors.New("application [id=" + id + "] not found")
	}

	// Check Placeholders and replace with values from request
	namespace := globals.ORCHESTRATOR_DEFAULT_NAMESPACE
	cpu := "50m"
	memory := "200Mi"
	if eventInfoMap["placeholders"] != nil {
		log.Info(pathLOG + "[processAppPlacement] Replacing 'placeholder' values ...")
		
		placeholdersList := eventInfoMap["placeholders"].([]interface{}) //.(map[string]interface{})
		e2coApp, err = parsers.E2coApplicationDeployment(&e2coApp, placeholdersList)

		log.Info(pathLOG + "[processAppPlacement] Getting values for namespace ...")
		for key := range placeholdersList {
			value := placeholdersList[key].(map[string]interface{})

			p := value["placeholder"].(string)
			v := value["value"].(string)

			if strings.Index(strings.ToLower(p), "namespace") >= 0 {
				namespace = v
			} else if strings.Index(strings.ToLower(p), "cpu") >= 0 {
				cpu = v
				if !strings.HasSuffix(cpu, "m") {
					cpu = cpu + "m"
				}
			} else if strings.Index(strings.ToLower(p), "memory") >= 0 {
				memory = v
				if !strings.HasSuffix(memory, "Mi") {
					memory = memory + "Mi"
				}
			}
		}
	} else {
		log.Info(pathLOG + "[processAppPlacement] No 'placeholder' values found")
		return errors.New("No 'placeholder' values found")
	}

	// App found
	var operacion structs.TaskOperation
	operacion.Operation = globals.MIGRATION
	operacion.Target = eventInfoMap["target_infra_id"].(string)
	operacion.TargetNamespace = namespace
	operacion.Memory = memory
	operacion.Cpu = cpu
	orchestrator.UpdateE2coApplication(&e2coApp, operacion) // UpdateE2coApplication(app *structs.E2coApplication, operation structs.TaskOperation) error

	return nil
}

// processAppScaleInOut Process App scale out (replicas > 0) or scale in (replicas < 0)
func processAppScaleInOut(id string, eventInfoMap map[string]interface{}, operation string) error {
	log.Info(pathLOG + "[processAppScaleInOut] Scaling in/out application ...")

	if id == "" {
		log.Warn(pathLOG + "[processAppScaleInOut] 'id' field is empty")
		return errors.New("application 'id' is empty")
	}

	// Check Placeholders and get replicas number
	var newreplicas int = 0
	if eventInfoMap["placeholders"] != nil {
		log.Info(pathLOG + "[processAppScaleInOut] Getting 'placeholder' values ...")
		placeholdersList := eventInfoMap["placeholders"].([]interface{}) //.(map[string]interface{})
		
		log.Info(pathLOG + "[processAppScaleInOut] Getting number of new replcias ...")
		for key := range placeholdersList {
			value := placeholdersList[key].(map[string]interface{})

			p := value["placeholder"].(string)
			v := value["value"].(string)

			if strings.Index(strings.ToLower(p), "replicas") >= 0 {
				newreplicas, _ = strconv.Atoi(v)
			}
		}
	} else {
		log.Info(pathLOG + "[processAppScaleInOut] No 'placeholder' values found")
		return errors.New("No 'placeholder' values found")
	}

	if newreplicas == 0 {
		return errors.New("New replica value not found")
	}

	// Call E2CO API / methods for app info
	e2coApp, err := orchestrator.GetE2coApplication(id) // GetE2coApplication(idTask string) (structs.E2coApplication, error)
	if err != nil { // error or app not found
		log.Error(pathLOG + "[processAppScaleInOut] App info not found!")
		return errors.New("application [id=" + id + "] not found")
	}

	// App found
	var operacion structs.TaskOperation
	operacion.Operation = globals.SCALE_OUT
	if operation == ACTION_SCALE_IN {
		operacion.Operation = globals.SCALE_IN
	}
	operacion.Replicas = newreplicas

	log.Debug(pathLOG + "[processAppScaleInOut] Application: " + e2coApp.ID + ", " + e2coApp.Name)
	log.Debug(pathLOG + "[processAppScaleInOut] Application update - Operation: " + operacion.Operation)
	log.Debug(pathLOG + "[processAppScaleInOut] New number of replicas: " + strconv.Itoa(operacion.Replicas))
	
	// check subapps replicas number
	for _, s := range e2coApp.Apps {
		subapp, err := db.ReadE2coSubApp(s.ID)

		log.Debug(pathLOG + "[processAppScaleInOut] Subapplication: " + subapp.ID + ", " + subapp.Name)
		log.Debug(pathLOG + "[processAppScaleInOut] Current number of replicas: " + strconv.Itoa(subapp.Replicas))

		if err != nil {
			log.Error(pathLOG+"[processAppScaleInOut] Error getting complete information from subapplication: ", err)
		}

		if (subapp.Replicas < newreplicas && operacion.Operation == "scale_out") || (subapp.Replicas > newreplicas && operacion.Operation == "scale_in") {
			orchestrator.UpdateE2coSubApp(&subapp, operacion)
		} else {
			log.Error(pathLOG + "[processAppScaleInOut] New number of replicas does not correspond to the operation")
		}
	}

	return nil
}

// processAppScaleUpDown Process App scale up / down
// https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/deployment-v1/#http-request-7
// https://kubernetes.io/docs/tasks/manage-kubernetes-objects/update-api-object-kubectl-patch/
// https://kubernetes.io/docs/tasks/manage-kubernetes-objects/update-api-object-kubectl-patch/#use-a-strategic-merge-patch-to-update-a-deployment
// ==> PATCH /apis/apps/v1/namespaces/{namespace}/deployments/{name}
func processAppScaleUpDown(id string, eventInfoMap map[string]interface{}, operation string) error {
	if id == "" {
		log.Warn(pathLOG + "[processAppScaleUpDown] 'id' field is empty")
		return errors.New("application 'id' is empty")
	}

	// Call E2CO API / methods for app info
	e2coApp, err := orchestrator.GetE2coApplication(id) // GetE2coApplication(idTask string) (structs.E2coApplication, error)
	if err != nil { // error or app not found
		log.Error(pathLOG + "[processAppScaleUpDown] App info [id=" + id + "] not found!")
		return errors.New("application [id=" + id + "] not found")
	}

	// Check Placeholders and get memory and cpu values
	var memory string = "0"
	var cpu string = "0"
	if eventInfoMap["placeholders"] != nil {
		log.Info(pathLOG + "[processAppScaleUpDown] Getting 'placeholder' values ...")
		placeholdersList := eventInfoMap["placeholders"].([]interface{}) 
		
		log.Info(pathLOG + "[processAppScaleUpDown] Getting values for memory and cpu ...")
		for key := range placeholdersList {
			value := placeholdersList[key].(map[string]interface{})

			p := value["placeholder"].(string)
			v := value["value"].(string)

			if strings.Index(strings.ToLower(p), "cpu") >= 0 {
				cpu = v
				if !strings.HasSuffix(cpu, "m") {
					cpu = cpu + "m"
				}
			} else if strings.Index(strings.ToLower(p), "memory") >= 0 {
				memory = v
				if !strings.HasSuffix(memory, "Mi") {
					memory = memory + "Mi"
				}
			}
		}
	} else {
		log.Info(pathLOG + "[processAppScaleUpDown] No 'placeholder' values found")
		return errors.New("No 'placeholder' values found")
	}

	// App found
	var operacion structs.TaskOperation
	operacion.Operation = globals.SCALE_UP
	operacion.Memory = memory
	operacion.Cpu = cpu
	if operation == ACTION_SCALE_DOWN {
		operacion.Operation = globals.SCALE_DOWN
	}
	orchestrator.UpdateE2coApplication(&e2coApp, operacion) // UpdateE2coApplication(app *structs.E2coApplication, operation structs.TaskOperation) error

	return nil
}

// processAppStart Process App start (deploy)
func processAppStart(id string, eventInfoMap map[string]interface{}) error {
	log.Info(pathLOG + "[processAppStart] Deploying application ...")

	if id == "" {
		log.Warn(pathLOG + "[processAppStart] 'id' is empty")
		return errors.New("application 'id' is empty")
	}

	// Call E2CO API / methods for app info
	log.Info(pathLOG + "[processAppStart] Getting application info ...")
	e2coApp, err := orchestrator.GetE2coApplication(id) // GetE2coApplication(idTask string) (structs.E2coApplication, error)
	if err != nil { // error or app not found
		log.Error(pathLOG + "[processAppStart] App info not found!")
		return errors.New("application [id=" + id + "] not found")
	}

	// set idE2coOrchestrator
	if eventInfoMap["target_infra_id"] != nil {
		log.Info(pathLOG + "[processAppStart] Setting infrastructure target [" + eventInfoMap["target_infra_id"].(string) + "] ...")
		e2coApp.Locations[0].IDE2cOrchestrator = eventInfoMap["target_infra_id"].(string)
		e2coApp.Apps[0].Locations[0].IDE2cOrchestrator = eventInfoMap["target_infra_id"].(string)
	}

	// Check Placeholders and replace with values from request
	if eventInfoMap["placeholders"] != nil {
		log.Info(pathLOG + "[processAppStart] Replacing 'placeholder' values ...")
		placeholdersList := eventInfoMap["placeholders"].([]interface{}) //.(map[string]interface{})
		e2coApp, err = parsers.E2coApplicationDeployment(&e2coApp, placeholdersList)
	} else {
		log.Info(pathLOG + "[processAppStart] No 'placeholder' values found")
	}
	
	// Deploy / launch App
	log.Info(pathLOG + "[processAppStart] Deploying application in cluster [" + e2coApp.Locations[0].IDE2cOrchestrator + "], namespace [" + e2coApp.Locations[0].NameSpace + "] ...")
 	e2coApp.DryRun = false
	err = orchestrator.DeployE2coApplication(&e2coApp, false)

	return nil
}

// processAppStop Process App stop (undeploy)
func processAppStop(id string) error {
	log.Info(pathLOG + "[processAppStop] Stopping application ...")

	if id == "" {
		log.Warn(pathLOG + "[processAppStop] 'id' is empty")
		return errors.New("application 'id' is empty")
	}

	// Call E2CO API / methods for app info
	if _, err := orchestrator.GetE2coApplication(id); err != nil { // error or app not found
		log.Error(pathLOG + "[processAppStop] App info not found!")
		return errors.New("application [id=" + id + "] not found")
	}

	// App found
	// Opinnable strategic:
	//	1. Call Delete API to remove app from infra (and from DB because a lateral effect).
	//	2. Create the app again (DB only) calling Post API but not deploy it?
	//  or scale to 0?
	
	// Delete App
	err := orchestrator.RemoveE2coApplication(id)

	// Create App
	//e2coApp.DryRun = true // Store only (not deploy)
	//orchestrator.DeployE2coApplication(&e2coApp, false) // DeployE2coApplication(application *structs.E2coApplication) error

	return err
}
