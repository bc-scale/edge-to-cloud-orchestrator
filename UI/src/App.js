//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 03 Mar 2021
//
// @author: ATOS
//
import React, { useState, Component} from 'react';
import { IntlProvider } from 'react-intl';
import Layout from './Layout';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/App.scss';


/**
 * App
 */
class App extends Component {

  /**
   * CONSTRUCTOR
   */
  constructor(props, context) {
    super(props, context);

    //Setting up global variables
    global.rest_api_lm = window.E2CO_UI_ENV_E2COEndpoint;
    if (global.rest_api_lm == null) {
      global.rest_api_lm = "http://localhost:8333/api/v1/";
    }
    console.log(global.rest_api_lm);

    global.const_rest_api_lm = window.E2CO_UI_ENV_E2COEndpoint;
    if (global.const_rest_api_lm == null) {
      global.const_rest_api_lm = "http://localhost:8333/api/v1/";
    }
    console.log(global.const_rest_api_lm);

    global.debug = true;
    
    console.log('Getting global.const_rest_api_lm ... ');
  }


  render() {
    var locale = 'en';
    var setLocale = 'en';

    return (
      <IntlProvider locale={locale}>
        <Layout setLocale={setLocale} />
      </IntlProvider>
    );
  }
}

export default App;
