//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 17 Sep 2021
// Updated on 17 Sep 2021
//
// @author: ATOS
//
package kafkadss

import (
	"errors"
	"encoding/json"
	"strconv"
	"strings"

	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
	//"atos.pledger/e2c-orchestrator/common/globals"
	"atos.pledger/e2c-orchestrator/data/manifests"

	"gopkg.in/yaml.v2"
)

/*
E2coApplicationDeployment
*/
func E2coApplicationDeployment(e2coApp *structs.E2coApplication, placeholdersList []interface{}) (structs.E2coApplication, error) {
	log.Debug(pathLOG + "[E2coApplicationDeployment] Preparing application object for deployment ...")
	log.Trace(pathLOG + "[E2coApplicationDeployment] placeholdersList: \n", placeholdersList)

	//if e2coApp.Apps[0].Type == globals.KUBERNETES { // "k8s"
	//	Kubernetes descriptor => YamlJSON
	var k8sDeployment manifests.K8SDeployment
	log.Trace(pathLOG+"[E2coApplicationDeployment] 'YamlJSON' ('deployDescriptor') content: \n" + e2coApp.Apps[0].YamlJSON)
	err := yaml.Unmarshal([]byte(e2coApp.Apps[0].YamlJSON), &k8sDeployment)
	if err != nil {
		log.Error(pathLOG+"[E2coApplicationDeployment] Error unmarshalling 'YamlJSON' info: ", err)
		if e, ok := err.(*json.SyntaxError); ok {
			log.Info("syntax error at byte offset ", e.Offset)
		}
		return structs.E2coApplication{}, errors.New("Error unmarshalling 'YamlJSON' info")
	}
	log.Trace(pathLOG+"[E2coApplicationDeployment] k8sDeployment object: \n", k8sDeployment)

	for key := range placeholdersList {
		value := placeholdersList[key].(map[string]interface{})

		p := value["placeholder"].(string)
		v := value["value"].(string)

		if strings.Index(strings.ToLower(p), "replicas") >= 0 {
			e2coApp.Replicas, _ = strconv.Atoi(v)
			e2coApp.Apps[0].Replicas, _ = strconv.Atoi(v)
		} else if strings.Index(strings.ToLower(p), "hostname") >= 0 {

		} else if strings.Index(strings.ToLower(p), "namespace") >= 0 {
			e2coApp.Locations[0].NameSpace = v
			e2coApp.Apps[0].Locations[0].NameSpace = v
		} else if strings.Index(strings.ToLower(p), "cpu") >= 0 {
			// TODO cpu and limits (k8s definition) vs hw (e2co application)
			// cpu - requests
			if len(k8sDeployment.Spec.Template.Spec.Containers) > 0 && len(k8sDeployment.Spec.Template.Spec.Containers[0].Resources.Requests.Cpu) > 0 {
				log.Trace(pathLOG+"[E2coApplicationDeployment] Replacing Cpu Requests [" + k8sDeployment.Spec.Template.Spec.Containers[0].Resources.Requests.Cpu + "] with [" + p + "] ...")
				e2coApp.Apps[0].Containers[0].Hw.Cpu = strings.Replace(k8sDeployment.Spec.Template.Spec.Containers[0].Resources.Requests.Cpu, p, v, 1)
			} else {
				e2coApp.Apps[0].Containers[0].Hw.Cpu = v
			}
			// cpu - limits
			/*if len(k8sDeployment.Spec.Template.Spec.Containers) > 0 && len(k8sDeployment.Spec.Template.Spec.Containers[0].Resources.Limits.Cpu) > 0 {
				log.Trace(pathLOG+"[E2coApplicationDeployment] Replacing Cpu Limits [" + k8sDeployment.Spec.Template.Spec.Containers[0].Resources.Limits.Cpu + "] with [" + p + "] ...")
				e2coApp.Apps[0].Containers[0].Hw.Cpu = strings.Replace(k8sDeployment.Spec.Template.Spec.Containers[0].Resources.Limits.Cpu, p, v, 1)
			} else {
				e2coApp.Apps[0].Containers[0].Hw.Cpu = v
			}*/
		} else if strings.Index(strings.ToLower(p), "memory") >= 0 {
			// memory - requests
			if len(k8sDeployment.Spec.Template.Spec.Containers) > 0 && len(k8sDeployment.Spec.Template.Spec.Containers[0].Resources.Requests.Memory) > 0 {
				log.Trace(pathLOG+"[E2coApplicationDeployment] Replacing Memory Requests [" + k8sDeployment.Spec.Template.Spec.Containers[0].Resources.Requests.Memory + "] with [" + p + "] ...")
				e2coApp.Apps[0].Containers[0].Hw.Memory = strings.Replace(k8sDeployment.Spec.Template.Spec.Containers[0].Resources.Requests.Memory, p, v, 1)
			} else {
				e2coApp.Apps[0].Containers[0].Hw.Memory = v
			}
			// memory - limits
			/*if len(k8sDeployment.Spec.Template.Spec.Containers) > 0 && len(k8sDeployment.Spec.Template.Spec.Containers[0].Resources.Limits.Memory) > 0 {
				log.Trace(pathLOG+"[E2coApplicationDeployment] Replacing Memory Limits [" + k8sDeployment.Spec.Template.Spec.Containers[0].Resources.Limits.Memory + "] with [" + p + "] ...")
				e2coApp.Apps[0].Containers[0].Hw.Memory = strings.Replace(k8sDeployment.Spec.Template.Spec.Containers[0].Resources.Limits.Memory, p, v, 1)
			} else {
				e2coApp.Apps[0].Containers[0].Hw.Memory = v
			}*/
		} else {

		}
	}
	//} else if e2coApp.Apps[0].Type == globals.DOCKER { //"docker"
	//	// 	Docker descriptor
	//	
	//}

	return *e2coApp, nil
}

// replaceStringValue
func replaceStringValue(id string, eventInfoMap map[string]interface{}, defaultValue string) string {
	log.Trace(pathLOG + "[replaceStringValue] - eventInfoMap['" + id + "']: \n", eventInfoMap[id])

	if eventInfoMap[id] != nil {
		m := eventInfoMap[id].(map[string]interface{})
		placeHolderName := m["placeholder"].(string)
		placeHolderValue := m["value"].(string)

		log.Debug(pathLOG + "[replaceStringValue] PLACEHOLDERS Name, Value: " + placeHolderName + ", " + placeHolderValue)

		return placeHolderValue
	}

	log.Warn(pathLOG + "[replaceStringValue] " + id + " - eventInfoMap['" + id + "'] is empty. Using default value [" + defaultValue + "] ...")
	return defaultValue
}


// replaceIntValue
func replaceIntValue(id string, eventInfoMap map[string]interface{}, defaultValue int) int {
	log.Trace(pathLOG + "[replaceIntValue] - eventInfoMap['" + id + "']: \n", eventInfoMap[id])

	if eventInfoMap[id] != nil{
		m := eventInfoMap[id].(map[string]interface{})
		placeHolderName := m["placeholder"].(string)

		placeHolderValue := 1
		switch m["value"].(type) { 
		case int, int32, int64, uint, uint32, uint64:
			placeHolderValue = m["value"].(int)
		case float64:
			placeHolderValue = int(m["value"].(float64))
		case float32:
			placeHolderValue = int(m["value"].(float32))
		default:
			log.Warn(pathLOG + "[E2coApplicationDeployment] Not expected type (float64) - replicasValue")
			placeHolderValue = defaultValue
		} 

		log.Debug(pathLOG + "[replaceIntValue] PLACEHOLDERS Name, Value: " + placeHolderName + ", " + strconv.Itoa(placeHolderValue))

		return placeHolderValue
	}

	log.Warn(pathLOG + "[replaceIntValue] " + id + " - eventInfoMap['" + id + "'] is empty. Using default value [" + strconv.Itoa(defaultValue) + "] ...")
	return defaultValue
}
