//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 04 Aug 2021
//
// @author: ATOS
//
package structs

import (
	"encoding/json"

	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/rest-api/sec/models"
)

// path used in logs
const pathLOG string = "E2CO > Data > structs "

///////////////////////////////////////////////////////////////////////////////
// REST API objects used in the calls

/*
ResponseE2CO JSON E2CO Response
*/
type ResponseE2CO struct {
	Resp         string      `json:"resp,omitempty"`
	Method       string      `json:"method,omitempty"`
	Message      string      `json:"message,omitempty"`
	E2COVersion  string      `json:"e2coversion,omitempty"`
	Content      string      `json:"content,omitempty"`
	List         interface{} `json:"list,omitempty"`
	Object       interface{} `json:"object,omitempty"`
	ExtraContent interface{} `json:"-"` // extraContent,omitempty
}

/*
TaskOperation JSON E2CO application Operation definition: migration, scaling out
Used to update a E2CO application
*/
type TaskOperation struct {
	Operation 			string      `json:"operation,omitempty"` // migration, scale_out, scale_in, scale_up (RAM, CPU ...), scale_down (RAM, CPU ...)
	Target    			string 	  	`json:"target,omitempty"`
	TargetNamespace    	string 	  	`json:"targetnamespace,omitempty"`
	From      			string 	  	`json:"from,omitempty"`
	Replicas  			int         `json:"replicas,omitempty"`
	Cpu       			string      `json:"cpu,omitempty"`
	Memory    			string      `json:"memory,omitempty"`
	Payload   			interface{} `json:"payload,omitempty"`
}

///////////////////////////////////////////////////////////////////////////////
// Operations with structs, like convert a struct to a string or transform
// one struct into another

/*
StringToE2cOrchestrator Parses a string to a 'E2cOrchestrator' struct
*/
func StringToE2cOrchestrator(ct string) (*E2cOrchestrator, error) {
	data := &E2cOrchestrator{}
	err := json.Unmarshal([]byte(ct), data)
	if err != nil {
		log.Error(pathLOG+"[StringToE2cOrchestrator] ERROR", err)
		return data, err
	}

	return data, nil
}

/*
StringToE2cOrchestratorApps Parses a string to a 'E2cOrchestratorApps' struct
*/
func StringToE2cOrchestratorApps(ct string) (*E2cOrchestratorApps, error) {
	data := &E2cOrchestratorApps{}
	err := json.Unmarshal([]byte(ct), data)
	if err != nil {
		log.Error(pathLOG+"[StringToE2cOrchestratorApps] ERROR", err)
		return data, err
	}

	return data, nil
}

/*
StringToE2coApplication Parses a string to a 'E2coApplication' struct
*/
func StringToE2coApplication(ct string) (E2coApplication, error) {
	data := E2coApplication{}
	err := json.Unmarshal([]byte(ct), &data)
	if err != nil {
		log.Error(pathLOG+"[StringToE2coApplication] ERROR", err)
		return data, err
	}

	return data, nil
}

/*
StringToE2coSubApp Parses a string to a 'E2coSubApp' struct
*/
func StringToE2coSubApp(ct string) (E2coSubApp, error) {
	data := E2coSubApp{}
	err := json.Unmarshal([]byte(ct), &data)
	if err != nil {
		log.Error(pathLOG+"[StringToE2coApp] ERROR", err)
		return data, err
	}

	return data, nil
}

/*
StringToE2coUser Parses a string to a 'models.User' struct
*/
func StringToE2coUser(ct string) (models.User, error) {
	data := models.User{}
	err := json.Unmarshal([]byte(ct), &data)
	if err != nil {
		log.Error(pathLOG+"[StringToE2coUser] ERROR", err)
		return data, err
	}

	return data, nil
}

/*
StringToE2coSoeObject Parses a string to a 'models.User' struct
*/
func StringToE2coSoeObject(ct string) (E2coSoeObject, error) {
	data := E2coSoeObject{}
	err := json.Unmarshal([]byte(ct), &data)
	if err != nil {
		log.Error(pathLOG+"[StringToE2coSoeObject] ERROR", err)
		return data, err
	}

	return data, nil
}