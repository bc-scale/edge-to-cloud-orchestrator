//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 19 Mar 2021
//
// @author: ATOS
//
package main

import (
	"encoding/json"
	"os"
	"strconv"
	"time"
	"strings"

	cfg "atos.pledger/e2c-orchestrator/common/cfg"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/db"
	"atos.pledger/e2c-orchestrator/eventhandler"
	restAPI "atos.pledger/e2c-orchestrator/rest-api"
)

// path used in logs
const pathLOG string = "## E2CO [Main] "

// fileExists checks if a file exists and is not a directory before we try using it to prevent further errors.
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

/*
 * Main: launch server
 */
func main() {
	log.Info("####################################################################################")
	log.Info(pathLOG + "Starting Pledger Edge-To-Cloud-Orchestration application (v0.3.0.20220323) ...")

	// set global properties from configuration file
	//cfgPath := "./resources/config/config.json" 
	//cfgPath := "./resources/config/config_mongo.json"
	//cfgPath := "./resources/config/config_empty.json" 

	cfgPath := "./resources/config/config_testbed.json" 

	// arguments: [1] ... alternative configuration file
	if len(os.Args) == 2 {
		log.Info(pathLOG + "Arguments[1] = " + os.Args[1])
		if fileExists(os.Args[1]) {
			log.Info(pathLOG + "Getting configuration from input parameter: " + os.Args[1] + " ...")
			cfgPath = os.Args[1]
		}
	}
	log.Info(pathLOG + "Getting configuration values from file [" + cfgPath + "] ...")

	cfg.Config = cfg.Configuration{}

	file, err := os.Open(cfgPath)
	if err != nil {
		log.Error(pathLOG+"ERROR", err)
		panic(err)
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&cfg.Config)
	if err != nil {
		log.Error(pathLOG+"ERROR", err)
		panic(err)
	}

	// check ENV values and modify global properties if specified
	log.Info(pathLOG + "Initializing Configuration struct ...")
	cfg.InitConfig(&cfg.Config)

	// show global configuration
	log.Info("..............................................................................")
	log.Info(pathLOG + "E2CO global configuration values: ")
	log.Info(pathLOG+"E2CO Version _________________ ", cfg.Config.E2COVersion)
	log.Info(pathLOG+"Server Port __________________ ", cfg.Config.E2COPort)
	log.Info(pathLOG+"Default REST API Endpoint ____ ", cfg.Config.DefaultRESTAPIEndPoint)
	log.Info(pathLOG+"SLA Framework ________________ ", cfg.Config.E2COSLALiteEndPoint)
	log.Info(pathLOG+"Pushgateway Endpoint _________ ", cfg.Config.E2COPrometheusPushgatewayEndPoint)
	log.Info(pathLOG+"Prometheus Endpoint __________ ", cfg.Config.E2COPrometheusEndPoint)
	log.Info(pathLOG+"DB: ")
	log.Info(pathLOG+"  Database ___________________ ", cfg.Config.Database)
	log.Info(pathLOG+"  Database Connection ________ ", cfg.Config.DatabaseConnection)
	log.Info(pathLOG+"  Database Repository ________ ", cfg.Config.DatabaseRepository)
	log.Info(pathLOG+"Configuration Service: ")
	log.Info(pathLOG+"  Warming time _______________ ", cfg.Config.E2CO_WARMING_TIME)
	log.Info(pathLOG+"  Endpoint ___________________ ", cfg.Config.E2CO_CONFIGSVC_ENDPOINT)
	log.Info(pathLOG+"  Credentials ________________ ", cfg.Config.E2CO_CONFIGSVC_CREDENTIALS)
	log.Info(pathLOG+"Kafka: ")
	log.Info(pathLOG+"  Client _____________________ ", cfg.Config.E2CO_KAFKA_CLIENT)
	log.Info(pathLOG+"  Endpoint ___________________ ", cfg.Config.E2CO_KAFKA_ENDPOINT)
	log.Info(pathLOG+"  VIOLATION Topic ____________ ", cfg.Config.E2CO_KAFKA_VIOLATION_TOPIC)
	log.Info(pathLOG+"  CONFIG Topic _______________ ", cfg.Config.E2CO_KAFKA_CONFIG_TOPIC)
	log.Info(pathLOG+"  DEPLOY Topic _______________ ", cfg.Config.E2CO_KAFKA_DEPLOY_TOPIC)
	log.Info(pathLOG+"  SOE Topic __________________ ", cfg.Config.E2CO_KAFKA_SOE_TOPIC) 
	log.Info(pathLOG+"  KEYSTORE Password __________ ", "******") //cfg.Config.E2CO_KAFKA_KEYSTORE_PASSWORD)
	log.Info(pathLOG+"  KEY Password _______________ ", "******") //cfg.Config.E2CO_KAFKA_KEY_PASSWORD)
	log.Info(pathLOG+"  TRUSTSTORE Password ________ ", "******") //cfg.Config.E2CO_KAFKA_TRUSTSTORE_PASSWORD)
	log.Info(pathLOG+"  KEYSTORE Location __________ ", cfg.Config.E2CO_KAFKA_KEYSTORE_LOCATION)
	log.Info(pathLOG+"  TRUSTSTORE Location ________ ", cfg.Config.E2CO_KAFKA_TRUSTSTORE_LOCATION)
	log.Info("..............................................................................")

	// DB
	log.Info(pathLOG + "Initializing database [" + cfg.Config.Database + "] ...")
	err = db.InitializeDB()
	if err != nil {
		log.Error(pathLOG + "Database adapter could not be initialized. Closing application ...")
		log.Fatal(err)
	}

	// users defined in Database
	users, err := db.GetUsers()
	if err != nil {
		log.Warn(pathLOG + "No users read from database")
	} else {
		log.Info(pathLOG + "List of users defined in Database: " + strconv.Itoa(len(users)))
		for i := range users {
			log.Info(pathLOG+"  => ", users[i].Name)
			log.Info(pathLOG+"     Email: ", users[i].Email)
		}
	}

	// clusters / orchestrators defined in Database
	orchs, err := db.ReadAllOrchestrators() // get orchestrators: ([]structs.E2cOrchestrator, error)
	if err != nil {
		log.Warn(pathLOG + "No orchestrators read from database")
	} else {
		log.Info(pathLOG + "List of orchestrators defined in Database: " + strconv.Itoa(len(orchs)))
		for i := range orchs {
			log.Info(pathLOG+"  => ", orchs[i].Name)
			log.Info(pathLOG+"     ID: ", orchs[i].ID)
			log.Info(pathLOG+"     Description: ", orchs[i].Description)
			log.Info(pathLOG+"     Type: ",orchs[i].Type)
			log.Info(pathLOG+"     Default DNamespace: ", orchs[i].DefaultNamespace)
			log.Info(pathLOG+"     S.O.: ", orchs[i].SO)
			log.Info(pathLOG+"     Host IP: ", orchs[i].IP)
			log.Info(pathLOG+"     REST API Endpoint: ", orchs[i].RESTAPIEndPoint)
			log.Info(pathLOG+"     Secondary SLALite Endpoint: ", orchs[i].SLALiteEndPoint)
			log.Info(pathLOG+"     Secondary Prometheus Pushgateway Endpoint: ", orchs[i].PrometheusPushgatewayEndPoint)
		}
	}

	// clusters / orchestrators defined in initial configuration
	if len(cfg.Config.Orchestrators) > 0 {
		log.Info(pathLOG + "List of orchestrators defined in configuration: " + strconv.Itoa(len(cfg.Config.Orchestrators)))
		for index := range cfg.Config.Orchestrators {
			log.Info(pathLOG+"  => ", cfg.Config.Orchestrators[index].E2cOrchestrator.Name)
			log.Info(pathLOG+"     ID: ", cfg.Config.Orchestrators[index].E2cOrchestrator.ID)
			log.Info(pathLOG+"     Description: ", cfg.Config.Orchestrators[index].E2cOrchestrator.Description)
			log.Info(pathLOG+"     Type: ", cfg.Config.Orchestrators[index].E2cOrchestrator.Type)
			log.Info(pathLOG+"     Default Namespace: ", cfg.Config.Orchestrators[index].E2cOrchestrator.DefaultNamespace)
			log.Info(pathLOG+"     S.O.: ", cfg.Config.Orchestrators[index].E2cOrchestrator.SO)
			log.Info(pathLOG+"     Host IP: ", cfg.Config.Orchestrators[index].E2cOrchestrator.IP)
			log.Info(pathLOG+"     REST API Endpoint: ", cfg.Config.Orchestrators[index].E2cOrchestrator.RESTAPIEndPoint)
			log.Info(pathLOG+"     Secondary SLALite Endpoint: ", cfg.Config.Orchestrators[index].E2cOrchestrator.SLALiteEndPoint)
			log.Info(pathLOG+"     Secondary Prometheus Pushgateway Endpoint: ", cfg.Config.Orchestrators[index].E2cOrchestrator.PrometheusPushgatewayEndPoint)
	
			// save Orchestatror to DB
			db.SetOrchestrator(cfg.Config.Orchestrators[index].E2cOrchestrator.ID, cfg.Config.Orchestrators[index].E2cOrchestrator)
		}
	}

	// initialize URLs
	log.Info(pathLOG + "Initializing URLs ...")
	cfg.InitializeURLs()
	log.Info(pathLOG + "Swagger UI :       http://localhost:" + strconv.Itoa(cfg.Config.E2COPort) + "/swaggerui/index.html")

	// Kafka events handler
	if strings.ToLower(cfg.Config.E2CO_KAFKA_CLIENT) == "none" {
		log.Warn(pathLOG + "No Kafka Event Handler defined.")
	} else  {
		log.Info(pathLOG + "Initializing Kafka Event Handler ...")
		eventhandler.NewEventHandler()
	}

	time.Sleep(2 * time.Second)

	// initialize REST API
	log.Info("####################################################################################")
	log.Info(pathLOG + "Initializing REST API (v2) ...")
	//restAPI.InitializeRESTAPI()
	restAPI.InitializeRESTAPIv2()
}
