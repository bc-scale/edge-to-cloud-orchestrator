#!/bin/bash
#PYTHON_PATH=
#CURL_PATH=
RABBITMQADMIN_PATH=./
RABBITMQ_USER=richardm
RABBITMQ_PASS=bigdatastack
RABBITMQ_HOSTNAME=rabbitmq
#RABBITMQ_HOSTNAME=localhost
RABBITMQ_PORT=15672

QOSEVA_HOST=localhost
QOSEVA_PORT=8090
QOSEVA_HOST=localhost
QOSEVA_PORT=32000
#QOSEVA_HOST=192.168.111.52
#QOSEVA_PORT=32000
QOSEVA_CHECKPERIOD=20

SAMPLE_AGREEMENT_PATH=./agreement.json
SAMPLE_AGREEMENT_ID=a03
SAMPLE_PLAYBOOK_PATH=./PlaybookSample.json
SAMPLE_PLAYBOOK_ID=grssr-recommendationprovider
SAMPLE_VIOLATION_PATH=./QoS_ViolationMessage.json
SAMPLE_QOS_START_PATH=./qos_start.json
SAMPLE_QOS_STOP_PATH=./qos_stop.json
QOSEVA_PID=1
SLA_APP_NAME=SLALite

E2CO_HOST=localhost
E2CO_PORT=8333
E2CO_HOST=localhost
E2CO_PORT=31000
#E2CO_HOST=192.168.111.52
#E2CO_PORT=31000
SAMPLE_CLUSTER_PATH=./e2co_cluster.json
SAMPLE_CLUSTER_ID=mycluster
SAMPLE_CLUSTER_PATH_2=./e2co_cluster_2.json
SAMPLE_CLUSTER_ID_2=mycluster2
SAMPLE_APP_PATH=./e2co_app.json
SAMPLE_APP_NAME=myapp
SAMPLE_APP_PATH_2=./e2co_app_2.json
SAMPLE_APP_NAME_2=myapp2
SAMPLE_NAMESPACE=core



function error() {
    echo -e "\e[101m[ERROR]\e[0m \e[91m$1\e[0m"
    exit -1
}


#--------------------------------------------------------------------
#		RABBITMQ SMOKE FUNCTIONS
#--------------------------------------------------------------------
function checkQueue() {
    echo "- Checking if $1 queue exists test ... "
	$RABBITMQADMIN_PATH/rabbitmqadmin.py -u $RABBITMQ_USER -p $RABBITMQ_PASS -H $RABBITMQ_HOSTNAME -P $RABBITMQ_PORT list queues | grep "$1"
	if [ $? -ne 0 ];
	then
	   error "The $1 queue does not exist!"
	fi
	return $?
}

function createQueue() {
    echo "- Creating $1 queue test ..."
	$RABBITMQADMIN_PATH/rabbitmqadmin.py -u $RABBITMQ_USER -p $RABBITMQ_PASS -H $RABBITMQ_HOSTNAME -P $RABBITMQ_PORT declare queue name=$1
	if [ $? -ne 0 ];
	then
	   error "Cannot create $1 queue!"
	fi
	return $?

}

function deleteQueue() {
    echo "- Deleting $1 queue test ..."
	$RABBITMQADMIN_PATH/rabbitmqadmin.py -u $RABBITMQ_USER -p $RABBITMQ_PASS -H $RABBITMQ_HOSTNAME -P $RABBITMQ_PORT delete queue name=$1 > /dev/null
	if [ $? -ne 0 ];
	then
	   error "Cannot delete $1 queue!"
	fi
	return $?
}

function purgeQueue() {
    echo "- Purging $1 queue test ..."
	$RABBITMQADMIN_PATH/rabbitmqadmin.py -u $RABBITMQ_USER -p $RABBITMQ_PASS -H $RABBITMQ_HOSTNAME -P $RABBITMQ_PORT purge queue name=$1 > /dev/null
	if [ $? -ne 0 ];
	then
	   error "Cannot purge $1 queue!"
	fi
	return $?
}

function checkQueueUP() {
    echo "- Checking RabbitMQ UP test ..."
	#echo $RABBITMQADMIN_PATH/rabbitmqadmin.py
	$RABBITMQADMIN_PATH/rabbitmqadmin.py -u $RABBITMQ_USER -p $RABBITMQ_PASS -H $RABBITMQ_HOSTNAME -P $RABBITMQ_PORT list queues > /dev/null
	if [ $? -ne 0 ];
	then
	   error "Cannot connect to RabbitMQ server!"
	fi
	return $?
}

function getQueueMsg() {
    echo "- Getting message from $1 queue test ..."
	$RABBITMQADMIN_PATH/rabbitmqadmin.py -u $RABBITMQ_USER -p $RABBITMQ_PASS -H $RABBITMQ_HOSTNAME -P $RABBITMQ_PORT get queue=$1 count=$2
	if [ $? -ne 0 ];
	then
	   error "Cannot get from $1 queue a message!"
	fi
	return $?
}

function pushQueueMsg() {
    echo "- Publishing message $2 into the $1 queue test ..."
	$RABBITMQADMIN_PATH/rabbitmqadmin.py -u $RABBITMQ_USER -p $RABBITMQ_PASS -H $RABBITMQ_HOSTNAME -P $RABBITMQ_PORT publish routing_key=$1 payload="$(<"$2")" > /dev/null
	if [ $? -ne 0 ];
	then
	   error "Cannot push into $1 queue a message!"
	fi
	return $?
}


#--------------------------------------------------------------------
#		SLA SMOKE FUNCTIONS
#--------------------------------------------------------------------
function checkSlaUP() {
    echo "- Checking $SLA_APP_NAME UP test ..."
    response=$(curl -k -X GET --write-out '%{http_code}' --silent --output /dev/null http://$QOSEVA_HOST:$QOSEVA_PORT/agreements) # > /dev/null
	#echo $response
	#if [ $? -ne 0 ];
	if [ $response -ne 200 ];
	then
	   error "Cannot connect to $SLA_APP_NAME server! [HTTP $response]"
	fi
	return $?
}

function checkSlaAll() {
    echo "- Checking ALL agreements test ..."
    curl -k -X GET --silent http://$QOSEVA_HOST:$QOSEVA_PORT/agreements | jq '.[] | {id:.id, name:.name, state:.state}'
    echo
    response=$(curl -k -X GET --write-out '%{http_code}' --silent --output /dev/null http://$QOSEVA_HOST:$QOSEVA_PORT/agreements) # > /dev/null
	#echo $response
	#if [ $? -ne 0 ];
	if [ $response -ne 200 ];
	then
	   error "Cannot connect to $SLA_APP_NAME server! [HTTP $response]"
	fi
	return $?
}

function createSla() {
    echo "- Creating agreement $1 test ..."
	curl -k -X DELETE --silent --output /dev/null http://$QOSEVA_HOST:$QOSEVA_PORT/agreements/$2
    response=$(curl -k -X POST --write-out '%{http_code}' --silent --output /dev/null -d @$1 http://$QOSEVA_HOST:$QOSEVA_PORT/agreements) # > /dev/null
	#echo $response
	#if [ $? -ne 0 ];
	if [ $response -ne 201 ];
	then
	   error "Cannot create agreement! [HTTP $response]"
	fi
	return $?
}

function checkSla() {
    echo "- Getting agreement $1 test ..."
    response=$(curl -k -X GET --write-out '%{http_code}' --silent --output /dev/null http://$QOSEVA_HOST:$QOSEVA_PORT/agreements/$1)
	#curl -k -X GET http://$QOSEVA_HOST:$QOSEVA_PORT/agreements/$1
    #curl -k -X GET http://$QOSEVA_HOST:$QOSEVA_PORT/agreements/$1/details
	#echo $response
	#if [ $? -ne 0 ];
	if [ $response -ne 200 ];
	then
	   error "Cannot get agreement $1! [HTTP $response]"
	fi
	return $?
}

function deleteSla() {
    echo "- Deleting agreement $1 test ..."
    response=$(curl -k -X DELETE --write-out '%{http_code}' --silent --output /dev/null http://$QOSEVA_HOST:$QOSEVA_PORT/agreements/$1) # > /dev/null
	#echo $response
	#if [ $? -ne 0 ];
	if [ $response -ne 204 ];
	then
	   error "Cannot delete agreement $1! [HTTP $response]"
	fi
	return $?
}

function deleteSlaAll() {
    curl -k -X GET --silent http://$QOSEVA_HOST:$QOSEVA_PORT/agreements | jq '.[] | .id' | while read d; do deleteSla $(echo $d | sed 's/"//g'); done
#    while read d; do searchAppByName $d $TEMP_KEY; done <<EOT
#$(curl -k -X GET --silent http://$E2CO_HOST:$E2CO_PORT/api/v1/apps | jq '.list[] | .id, .name')
#EOT
}


function startSla() {
    echo "- Resuming agreement $1 test ..."
        response=$(curl -k -X PUT --write-out '%{http_code}' --silent --output /dev/null http://$QOSEVA_HOST:$QOSEVA_PORT/agreements/$1/start)
	#echo $response
	#if [ $? -ne 0 ];
	if [ $response -ne 200 ];
	then
	   error "Cannot resume agreement $1! [HTTP $response]"
	fi
	return $?
}

function stopSla() {
    echo "- Pausing agreement $1 test ..."
        response=$(curl -k -X PUT --write-out '%{http_code}' --silent --output /dev/null http://$QOSEVA_HOST:$QOSEVA_PORT/agreements/$1/stop)
	#echo $response
	#if [ $? -ne 0 ];
	if [ $response -ne 204 ];
	then
	   error "Cannot pause agreement $1! [HTTP $response]"
	fi
	return $?
}

function checkCollectMetricLog() {
    echo "- Checking Start/Stop collecting metric log for $1 test ..."
	echo "   - Waiting $SLA_APP_NAME job next cycle for $QOSEVA_CHECKPERIOD sec for $1 message ..."
	timeout $QOSEVA_CHECKPERIOD sudo strace -p $QOSEVA_PID -f -s1000 -e write 2>&1 | grep -m 1 -q $1
	if [ $? -ne 0 ];
	then
		error "Cannot get $1 from log!"
	fi
	return $?
}

function checkCollectMetricMsg() {
    echo "- Checking Start/Stop collecting metric message from $1 queue test ..."
	echo "   - Waiting $SLA_APP_NAME job next cycle for $QOSEVA_CHECKPERIOD sec for $2 message ..."
	#sleep $QOSEVA_CHECKPERIOD
	i="0"
	while [ $i -lt $[$QOSEVA_CHECKPERIOD] ]
	#while [ true ]
	do
	    #getQueueMsg $1 -1
		getQueueMsg $1 -1 | grep $2
		#if [ $? -ne 0 ];
		#then
		#   error "Cannot get message $2 from $1 queue!"
		#fi
		if [ $? -eq 0 ];
		then
		    echo ""
			return $?
		fi
		i=$[$i+1]
		echo -n "."
		sleep 1
	done
	if [ $i -eq $QOSEVA_CHECKPERIOD ];
	then
	    echo ""
		error "Cannot get message $2 from $1 queue!"
	fi
	return $?
}

function checkQoSViolationMsg() {
    echo "- Checking QoS Violation message ($3) with $1 queue test ..."
	echo "   - Pushing message into $1 queue ..."
	pushQueueMsg $1 $3 > /dev/null
	if [ $? -ne 0 ];
	then
	   error "Cannot push message $2 into $1 queue!"
	fi
	echo "   - Getting message from $1 queue ..."
	getQueueMsg $1 1 | grep $2
	if [ $? -ne 0 ];
	then
	   error "Cannot get message $2 from $1 queue!"
	fi
	purgeQueue $1
	return $?
}


#--------------------------------------------------------------------
#		RABBITMQ SMOKE TESTS
#--------------------------------------------------------------------
function smokeTestRabbitMQ() {
	echo -e "\e[42mChecking RabbitMQ ...\e[0m"
	#createQueue "OrchestratorQOSFeed"
	#createQueue "EnrichedPlaybookFeed"
	checkQueueUP
	checkQueue "manager"
	checkQueue "qos"
	checkQueue "OrchestratorQOSFeed"
	checkQueue "EnrichedPlaybookFeed"
	#deleteQueue "OrchestratorQOSFeed"
	#deleteQueue "EnrichedPlaybookFeed"
	echo -e "\e[92mRabbitMQ is OK!\e[0m"
	return $?
}


#--------------------------------------------------------------------
#		SLA SMOKE TESTS
#--------------------------------------------------------------------
function smokeTestQoSEvaluator() {
	echo -e "\e[42mChecking $SLA_APP_NAME ...\e[0m"
	checkSlaUP
	##purgeQueue "manager"	# This is only to debug the test. Comment it when finish.
	##deleteSla $SAMPLE_AGREEMENT_ID
	createSla $SAMPLE_AGREEMENT_PATH $SAMPLE_AGREEMENT_ID
	checkSla $SAMPLE_AGREEMENT_ID
	##pushQueueMsg "manager" $SAMPLE_QOS_START_PATH
	##getQueueMsg "manager" -1 | grep "qos_start"
	#checkCollectMetricMsg "manager" "qos_start" $QOSEVA_CHECKPERIOD
	#checkCollectMetricLog "qos_start" $QOSEVA_CHECKPERIOD
	##sleep $QOSEVA_CHECKPERIOD
#	stopSla $SAMPLE_AGREEMENT_ID
	##pushQueueMsg "manager" $SAMPLE_QOS_STOP_PATH
	##getQueueMsg "manager" -1 | grep "qos_stop"
	#checkCollectMetricLog "qos_stop" $QOSEVA_CHECKPERIOD	·
	#checkCollectMetricMsg "manager" "qos_stop" $QOSEVA_CHECKPERIOD
	##sleep $QOSEVA_CHECKPERIOD
	#startSla $SAMPLE_AGREEMENT_ID
	#checkCollectMetricMsg "manager" "qos_start" $QOSEVA_CHECKPERIOD
#	deleteSla $SAMPLE_AGREEMENT_ID
	#checkQoSViolationMsg "OrchestratorQOSFeed" "QoS_Violation" $SAMPLE_VIOLATION_PATH
	#purgeQueue "qos"
	##purgeQueue "manager"
	echo -e "\e[92m$SLA_APP_NAME is OK!\e[0m"
	return $?
}


#--------------------------------------------------------------------
#		SLA PROXY SMOKE TESTS
#--------------------------------------------------------------------
function smokeTestQoSEvaProxy() {
	echo -e "\e[42mChecking $SLA_APP_NAME Proxy ...\e[0m"
	checkQueueUP
	checkQueue "manager"
	checkQueue "EnrichedPlaybookFeed"
	checkSlaUP
	pushQueueMsg "EnrichedPlaybookFeed" $SAMPLE_PLAYBOOK_PATH
	#getQueueMsg "EnrichedPlaybookFeed" -1
	#purgeQueue "EnrichedPlaybookFeed"
	checkSla $SAMPLE_PLAYBOOK_ID
	deleteSla $SAMPLE_PLAYBOOK_ID
	#purgeQueue "manager"
	echo -e "\e[92m$SLA_APP_NAME Proxy is OK!\e[0m"
	return $?
}


#--------------------------------------------------------------------
#		E2CO SMOKE FUNCTIONS
#--------------------------------------------------------------------
function createCluster() {
    echo "- Creating cluster $2 test ..."
        response=$(curl -k -X POST --write-out '%{http_code}' --silent --output /dev/null -d @$1 http://$E2CO_HOST:$E2CO_PORT/api/v1/ime/e2co) # > /dev/null
	if [ $response -ne 200 ];
	then
	   error "Cannot create cluster! [HTTP $response]"
	fi
	return $?
}

function checkCluster() {
    echo "- Getting cluster $1 test ..."
	curl -k -X GET --write-out '%{http_code}' --silent http://$E2CO_HOST:$E2CO_PORT/api/v1/ime/e2co/$1
	echo
        response=$(curl -k -X GET --write-out '%{http_code}' --silent --output /dev/null http://$E2CO_HOST:$E2CO_PORT/api/v1/ime/e2co/$1)
	if [ $response -ne 200 ];
	then
	   error "Cannot get cluster $1! [HTTP $response]"
	fi
	return $?
}

function checkClusterAll() {
    echo "- Getting ALL clusters test ..."
	curl -k -X GET --write-out '%{http_code}' --silent http://$E2CO_HOST:$E2CO_PORT/api/v1/ime/e2co
	echo
        response=$(curl -k -X GET --write-out '%{http_code}' --silent --output /dev/null http://$E2CO_HOST:$E2CO_PORT/api/v1/ime/e2co)
	if [ $response -ne 200 ];
	then
	   error "Cannot get ALL clusters! [HTTP $response]"
	fi
	return $?
}

function checkE2COStatus() {
    echo "- Getting E2CO status test ..."
	curl -k -X GET --write-out '%{http_code}' --silent http://$E2CO_HOST:$E2CO_PORT/api/v1/
	echo
        response=$(curl -k -X GET --write-out '%{http_code}' --silent --output /dev/null http://$E2CO_HOST:$E2CO_PORT/api/v1/)
	if [ $response -ne 200 ];
	then
	   error "Cannot get E2CO status! [HTTP $response]"
	fi
	return $?
}

function deleteCluster() {
    echo "- Deleting cluster $1 test ..."
        response=$(curl -k -X DELETE --write-out '%{http_code}' --silent --output /dev/null http://$E2CO_HOST:$E2CO_PORT/api/v1/ime/e2co/$1) # > /dev/null
	if [ $response -ne 200 ];
	then
	   error "Cannot delete cluster $1! [HTTP $response]"
	fi
	return $?
}

function deleteAllFromCluster() {
    deleteCluster $1
    return $? 
}

function createApp() {
    echo "- Creating app $2 test ..."
	#curl -k -X POST -d @$1 http://$E2CO_HOST:$E2CO_PORT/api/v1/apps 
    #echo
    response=$(curl -k -X POST --write-out '%{http_code}' --silent --output /dev/null -d @$1 http://$E2CO_HOST:$E2CO_PORT/api/v1/apps) # > /dev/null
	if [ $response -ne 200 ];
	then
	   error "Cannot create app! [HTTP $response]"
	fi
	return $?
}

function searchAppByName {
    #echo "searchAppByName: $1 - $2"
    if [ -z $TEMP_ID ]; then
       TEMP_ID=$1;
    else
        #echo "$1 == $2"
        if  [ "$1" = \"$2\" ]; then
           TEMP_ID_FOUND=$TEMP_ID
           #echo "searchAppByName found: $1 == $2 => $TEMP_ID_FOUND!"
        fi
        TEMP_ID=""
    fi 
    #echo "searchAppByName result: $TEMP_ID_FOUND - $TEMP_ID"
}

function getAppID() {
    #echo "getAppID: $1"
    local TEMP_KEY="$1"
    local TEMP_ID=""
    local TEMP_ID_FOUND=""
    #curl -k -X GET --silent http://$E2CO_HOST:$E2CO_PORT/api/v1/apps | jq '.list[] | .id, .name' | while read d; do searchAppByName $d $TEMP_KEY; done 
    while read d; do searchAppByName $d $TEMP_KEY; done <<EOT
$(curl -k -X GET --silent http://$E2CO_HOST:$E2CO_PORT/api/v1/apps | jq '.list[] | .id, .name')
EOT
    TEMP_ID=
    TEMP_KEY=
    #echo "getAppID result: $TEMP_ID_FOUND"
    echo $TEMP_ID_FOUND | sed 's/"//g'
}

function checkApp() {
    echo "- Getting app $1 test ..."
    result=$(getAppID $1)
    #echo $result
    if [ -z $result ];
    then
       return $?
    fi    
	#curl -k -X GET --silent http://$E2CO_HOST:$E2CO_PORT/api/v1/apps/$1
    curl -k -X GET --silent http://$E2CO_HOST:$E2CO_PORT/api/v1/apps/$result
	echo
    #response=$(curl -k -X GET --write-out '%{http_code}' --silent --output /dev/null http://$E2CO_HOST:$E2CO_PORT/api/v1/apps/$1)
    response=$(curl -k -X GET --write-out '%{http_code}' --silent --output /dev/null http://$E2CO_HOST:$E2CO_PORT/api/v1/apps/$result)
	if [ $response -ne 200 ];
	then
	   error "Cannot get app $1! [HTTP $response]"
	fi
	return $?
}

function checkAppAll() {
    echo "- Getting ALL apps test ..."
	curl -k -X GET --write-out '%{http_code}' --silent http://$E2CO_HOST:$E2CO_PORT/api/v1/apps
	echo
    response=$(curl -k -X GET --write-out '%{http_code}' --silent --output /dev/null http://$E2CO_HOST:$E2CO_PORT/api/v1/apps)
	if [ $response -ne 200 ];
	then
	   error "Cannot get ALL apps! [HTTP $response]"
	fi
	return $?
}

function deleteApp() {
    echo "- Deleting app $1 test ..."
    result=$(getAppID $1)
    echo "App ID: $result"
    if [ -z $result ];
    then
       return $?
    fi
    #response=$(curl -k -X DELETE --write-out '%{http_code}' --silent --output /dev/null http://$E2CO_HOST:$E2CO_PORT/api/v1/apps/$1) # > /dev/null
    response=$(curl -k -X DELETE --write-out '%{http_code}' --silent --output /dev/null http://$E2CO_HOST:$E2CO_PORT/api/v1/apps/$result) # > /dev/null
	if [ $response -ne 200 ];
	then
	   error "Cannot delete app $1! [HTTP $response]"
	fi
	return $?
}

function deleteAllFromApp() {
    echo "- Deleting ALL instances of app $1 test ..."
    local result=$(getAppID $1)
    #if [ "$result" == "" ];
    #then
    #   unset result
    #fi
    while [ ! -z $result ]
    do
       deleteApp $1
       sleep 1
       #checkAppAll
       result=$(getAppID $1)
       echo "Next to delete: $result"
    done
    return $?
}

function checkK8SAPIToken() {
    echo "- Checking K8S API call for cluster $1 test ..."
    # Check all possible clusters, as your .KUBECONFIG may have multiple contexts:
    kubectl config view -o jsonpath='{"Cluster name\tServer\n"}{range .clusters[*]}{.name}{"\t"}{.cluster.server}{"\n"}{end}'
    # Select name of cluster you want to interact with from above output:
    CLUSTER_NAME="$1"
    # Point to the API server referring the cluster name
    APISERVER=$(kubectl config view -o jsonpath="{.clusters[?(@.name==\"$CLUSTER_NAME\")].cluster.server}")
    # Gets the token value
    DEFAULT_NAMESPACE="core"
    DEFAULT_ACCOUNT="default"
    TOKEN=$(kubectl -n $DEFAULT_NAMESPACE get secrets -o jsonpath="{.items[?(@.metadata.annotations['kubernetes\.io/service-account\.name']==\"$DEFAULT_ACCOUNT\")].data.token}"|base64 --decode)
    #echo TOKEN=$TOKEN
    echo "Using service-account [$DEFAULT_ACCOUNT] in namespace [$DEFAULT_NAMESPACE]... $(kubectl -n $DEFAULT_NAMESPACE describe secret default-token | grep token:)" 
    curl -X GET --silent $APISERVER/api --header "Authorization: Bearer $TOKEN" --insecure
    #curl -X GET --silent https://0.0.0.0:6443/apis/apps/v1/namespaces/default/deployments --header "Authorization: Bearer $TOKEN" --insecure
    echo
    return $?
}

function waitForAppReady() {
    echo "Waiting for app $1 ready ..."
    #kubectl -n core rollout status deployment myapp
    #echo kubectl -n core wait --for=condition=ready pod -l app=$(getAppID $SAMPLE_APP_NAME) 
    kubectl -n $SAMPLE_NAMESPACE wait --for=condition=ready --timeout=60s pod -l app=$(getAppID $1)
    #sleep 60
    #kubectl get all --all-namespaces -o wide
    kubectl -n $SAMPLE_NAMESPACE get all -o wide
    return $?
}


#--------------------------------------------------------------------
#		E2CO SMOKE TEST
#--------------------------------------------------------------------
function smokeTestE2CO() {
    echo -e "\e[42mChecking E2CO ...\e[0m"

    #checkSlaAll
    deleteSlaAll

    checkE2COStatus
    deleteAllFromApp $SAMPLE_APP_NAME
    deleteAllFromApp $SAMPLE_APP_NAME_2
    deleteAllFromCluster $SAMPLE_CLUSTER_ID
    deleteAllFromCluster $SAMPLE_CLUSTER_ID_2

    createCluster $SAMPLE_CLUSTER_PATH $SAMPLE_CLUSTER_ID
    #checkCluster $SAMPLE_CLUSTER_ID
    createCluster $SAMPLE_CLUSTER_PATH_2 $SAMPLE_CLUSTER_ID_2
    #checkCluster $SAMPLE_CLUSTER_ID_2
    #checkClusterAll

    #deleteAllFromApp $SAMPLE_APP_NAME
    #checkK8SAPIToken "kind-kind" $SAMPLE_CLUSTER_ID
    #createApp $SAMPLE_APP_PATH $SAMPLE_APP_NAME
    #checkApp $SAMPLE_APP_NAME
    #deleteAllFromApp $SAMPLE_APP_NAME_2
    createApp $SAMPLE_APP_PATH_2 $SAMPLE_APP_NAME_2
    #checkApp $SAMPLE_APP_NAME_2
    #checkAppAll

    #waitForAppReady $SAMPLE_APP_NAME

    #sleep 5
    #checkSla $(getAppID $SAMPLE_APP_NAME)

    #deleteApp $SAMPLE_APP_NAME
    #deleteApp $SAMPLE_APP_NAME_2
    #checkApp $SAMPLE_APP_NAME
    #sleep 1
    #checkAppAll

    #deleteCluster $SAMPLE_CLUSTER_ID_2
    #deleteCluster $SAMPLE_CLUSTER_ID
    #checkClusterAll

    #sleep 5
    #checkSlaAll

    echo -e "\e[92mChecking E2CO is OK!\e[0m"
}


#--------------------------------------------------------------------
#		MAIN SMOKE TESTS
#--------------------------------------------------------------------
if [ "$1" = "rabbit" ];
then
	smokeTestRabbitMQ
	exit 0
fi
if [ "$1" = "qos" ];
then
	smokeTestQoSEvaluator
	exit 0
fi
if [ "$1" = "proxy" ];
then
	smokeTestQoSEvaProxy
	exit 0
fi
if [ "$1" = "e2co" ];
then
	smokeTestE2CO
	exit 0
fi
echo -e "\e[92mSTARTING smoke test ...\e[0m"
#smokeTestRabbitMQ
#smokeTestQoSEvaluator
#smokeTestQoSEvaProxy
smokeTestE2CO
echo -e "\e[92mSmoke test PASSED!\e[0m"
